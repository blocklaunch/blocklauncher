package org.blocklaunch;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import net.minecraft.launcher.game.MinecraftReleaseType;

import com.mojang.launcher.versions.Version;

public class BLUtil {
	
	public static boolean isBL(String id){
		if (id.startsWith("BL")){
			return true;
		}else{		
		return false;
		}
	}
	
	public static boolean isAddonFile(File addon) throws ZipException, IOException {
		if (!addon.getName().endsWith(".zip") && !addon.getName().endsWith(".jar"))
			return false;
		
		ZipFile addonzip = new ZipFile(addon);
		ZipEntry addoninfo = addonzip.getEntry("addon.info");
		addonzip.close();
		return addoninfo != null;
	}
	
	public static File[] concatFileArrays(File[] a, File[] b) {
		
		File[] together;
		together = new File[a.length + b.length];
		System.arraycopy(a, 0, together, 0, a.length);
		System.arraycopy(b, 0, together, a.length, b.length);
		
		return together;
	}
	
	public static String[] getMods(String url){
		String line;
		String[] array = null;
		List liste = null;
		
		try{
			Scanner scanner = new Scanner(new URL(url).openStream()); //�ffnet dateiscanner

		    
		//	FileReader fread = new FileReader(url); //�ffntet die gew�nschte txt Datei
		//	BufferedReader in = new BufferedReader(fread); // txt wird in den BufferReader geladen
			
			
			for(int i = 0;scanner.hasNextLine(); i++){ // durchlaufen, bis line = null(datei zu ende ist)
				line = scanner.nextLine().toString();
				if (!(line.startsWith("#"))){
					liste.add(line);
				} // array[i] wird mit line(der aktuellen Zeile) belegt			
			}
			array = (String[]) liste.toArray();
		}
		catch(IOException e){
			System.out.println("IO-Fehler!");
		}
		
		
		System.out.println("Mods:" +array); // nur der Test, ob das array mit der Zeile 1 beschrieben wurde...
		
		if (liste.isEmpty()){
			return null;
		}else{
		return array;}
	}
	
	public static List<String> getModList(String url) {
		List<String> list = new ArrayList<String>();
		BufferedReader reader = null;
		try {
			URL url2 = new URL(url);
			reader = new BufferedReader(new InputStreamReader(url2.openStream()));
			
			//read line by line
			String s;
			while ((s = reader.readLine()) != null)   {
				if (s.startsWith("#")) continue; //ignore comments
				list.add(s); //add to list
			}
			return list;
		 } catch (Exception e) {
			 e.printStackTrace();
			 return null;
		 } finally {
			 if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }
		 }
	}

	public static String renameVersions(Version version){
		//String hereversion = version.getType().getName();
        String ID_raw = version.getId();
    
        String ID = "Minecraft " + ID_raw;
        if (ID_raw.startsWith("BL")){
        	String[] Parts = ID_raw.split(Pattern.quote("_"));
        	if (version.getType() == MinecraftReleaseType.RELEASE){
        		ID = "BlockLaunch "+Parts[1]+" (Minecraft "+Parts[2]+")";
            }else{
        	if (version.getType() == MinecraftReleaseType.SNAPSHOT){
        		ID = "BlockLaunch Snapshot "+Parts[2];
        	}}}
        else{
        	if (version.getType() == MinecraftReleaseType.OLD_ALPHA){
        		if (ID_raw.startsWith("a")){
        			ID = "Minecraft Alpha "+ID_raw.substring(1, ID_raw.length());
        		}else{
        		if (ID_raw.startsWith("inf")){
            		ID = "Minecraft Infdev "+ID_raw.substring(4, ID_raw.length());
            	}else{
            	if (ID_raw.startsWith("c")){
                	ID = "Minecraft Classic "+ID_raw.substring(1, ID_raw.length());
                }else{
                if (ID_raw.startsWith("rd")){
                	ID = "Minecraft Pre-Classic 0.0.9a ("+ID_raw+")";                    
                }else{
                	ID = "Minecraft Alpha "+ID_raw;
                }}}}
        	}else{
        	if (version.getType() == MinecraftReleaseType.OLD_BETA){
        		ID_raw = ID_raw.substring(1, ID_raw.length());        		
            	ID = "Minecraft Beta "+ID_raw;
            }else{
        	if (version.getType() == MinecraftReleaseType.RELEASE){
            	ID = "Minecraft "+ID_raw;
            }else{
        	if (version.getType() == MinecraftReleaseType.SNAPSHOT){
            	ID = "Minecraft Snapshot "+ID_raw;
        	}}}}}
        return ID;
	}
	
	public static String generateID(){
		DateFormat formatter = new SimpleDateFormat("yyyyMMWWuHHmmssSSS");
		String date = formatter.format(new Date());		
		return date;		
	}
	
}
