package org.blocklaunch.start;

import net.minecraft.launcher.Launcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class Util {
    public static final String APPLICATION_NAME = "blocklaunch";
    public static String propFileLocation = Util.getWorkingDirectory() + "/BLauncher.properties";

    public static String getProperties(String key) {
        String value = "";
        File propFile = new File(propFileLocation);
        Properties prop = new Properties();
        if (!propFile.exists()) {
            return value;
        } else {
            try {
                prop.load(new FileInputStream(propFile));
            } catch (IOException ex) {
                Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            value = prop.getProperty(key);
        }

        return value;
    }
    
    

    public static Object setProperties(String key, String value) {
        Object finalValue = "";
        File propFile = new File(propFileLocation);
        Properties prop = new Properties();
        if (!propFile.exists()) {
            return finalValue;
        } else {
            try {
                prop.load(new FileInputStream(propFile));
            } catch (IOException ex) {
                Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
            }
            finalValue = prop.setProperty(key, value);
            try {
                prop.store(new FileOutputStream(propFile), "BLauncher");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return finalValue;
    }

    public static void restartApplication() throws URISyntaxException, IOException {
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File currentJar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        System.out.println(currentJar.getAbsolutePath());
  
        if (currentJar.getName().endsWith("exe")) {

            final ArrayList<String> command = new ArrayList<String>();
            // command.add(javaBin);
            //command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
            System.exit(0);
        }

        if (!currentJar.getName().endsWith(".jar"))
            return;

  
        final ArrayList<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-jar");
        command.add(currentJar.getPath());

        final ProcessBuilder builder = new ProcessBuilder(command);
        builder.start();
        System.exit(0);
    }

    public static OS getPlatform() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) return OS.WINDOWS;
        if (osName.contains("mac")) return OS.MACOS;
        if (osName.contains("linux")) return OS.LINUX;
        if (osName.contains("unix")) return OS.LINUX;
        return OS.UNKNOWN;
    }

    public static File getWorkingDirectory() {
    	return Util.getWorkingDirectory("blocklaunch");
    }

    public static File getWorkingDirectory(String name) {
        String userHome = System.getProperty("user.home", ".");
        File workingDirectory;
        OS os = getPlatform();
    //    System.out.println("OS:"+os.toString());
        if (os == OS.LINUX)
        	{
            workingDirectory = new File(userHome, "."+ name + "/");
        	}
        else{
        	if (os == OS.WINDOWS)
        	{
        		String applicationData = System.getenv("APPDATA");
        		String folder = applicationData != null ? applicationData : userHome;
        		workingDirectory = new File(folder, "."+ name + "/");
        	}else{
        		if (os == OS.MACOS){
        			workingDirectory = new File(userHome, "Library/Application Support/"+ name);
        		}else{
        			workingDirectory = new File(userHome, name + "/");}
        		}
        	}
    
        
        
//        switch (getPlatform().ordinal()) {
//            case 1:
//            case 2:
//                workingDirectory = new File(userHome, ".minecraft/");
//                break;
//            case 3:
//                String applicationData = System.getenv("APPDATA");
//                String folder = applicationData != null ? applicationData : userHome;
//
//                workingDirectory = new File(folder, ".minecraft/");
//                break;
//            case 4:
//                workingDirectory = new File(userHome, "Library/Application Support/minecraft");
//                break;
//            default:
//                workingDirectory = new File(userHome, "minecraft/");
//        }

        return workingDirectory;
    }
    
    

    public static enum OS {
        WINDOWS, MACOS, SOLARIS, LINUX, UNKNOWN;
    }
    
    public static final HyperlinkListener EXTERNAL_HYPERLINK_LISTENER = new HyperlinkListener() {
		public void hyperlinkUpdate(HyperlinkEvent paramAnonymousHyperlinkEvent) {
			if (paramAnonymousHyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
				try {
					openLink(paramAnonymousHyperlinkEvent.getURL().toURI());
				} catch (Exception localException) {
					localException.printStackTrace();
				}
		}
	};
	
	public static void openLink(URI paramURI) {
		try {
			Object localObject = Class.forName("java.awt.Desktop")
					.getMethod("getDesktop", new Class[0])
					.invoke(null, new Object[0]);
			localObject.getClass()
					.getMethod("browse", new Class[] { URI.class })
					.invoke(localObject, new Object[] { paramURI });
		} catch (Throwable localThrowable) {
			System.out.println("Failed to open link " + paramURI.toString());
		}
	}
}
