package org.blocklaunch;

import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.blocklaunch.start.Util;

import com.kirby0016.ModInstaller.ModIO;

import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.profile.Profile;

public class AddMods {
	
	static Logger LOGGER = LogManager.getLogger();
	
	public static void addBaseMods(String id, File gamefolder){
		System.out.println("Version:"+id);
		System.out.println("Version:"+gamefolder);
		String versionfolder = ("versions/"+id);
		
		File jar0 = new File(gamefolder, versionfolder);
		String jar1 = jar0 +File.separator+ id+".jar";
		System.out.println(jar1);
		
		String tempfolder = (versionfolder+"/basemods/");
		File modsfolder = new File(gamefolder, tempfolder);	
		
		System.out.println("122  "+String.valueOf(modsfolder));
		
		File[]Modslist = modsfolder.listFiles();
		
		System.out.println(Modslist);
		
		//String dummy;
		for(File f : Modslist) {
			LOGGER.info("Adding " + f.getName());		    
		  //  dummy = jar1 + f.
		    try {
				ModIO.addmod(f.getAbsolutePath(), new File(jar1));
			} catch (Exception e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			}
		}
		
		
	}
	/**
	 * backups the mods of .minecraft/mods folder to mods_backup
	 * @param launcher the launcher for logging
	 * @param restore if false, backups mods, if true, restores those
	 */
	public static void backupMCMods(Launcher launcher, boolean restore){
		
		File gamefolder = Util.getWorkingDirectory("minecraft");
		
		System.out.println(gamefolder);
		
		String fmods = "mods/";
		File mods = new File(gamefolder, fmods);
		String fmods_backup = "mods_backup/";
		File mods_backup = new File(gamefolder, fmods_backup);		
		//deldir(mods);
		
		if (mods.exists()){		
		    try {
		      if (!restore){
		    	  try{
		    		  deldir(mods_backup);
		    	  }finally{
		    		  ModIO.copyFolder(mods, mods_backup);
		    	  }
		      }else{
		    	deldir(mods);
		    	try{
		    		ModIO.copyFolder(mods_backup, mods);
		    	}finally{
		    		deldir(mods_backup);
		    		mods_backup.delete();
		    	}
		      }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void addotherMods(String id, File gamefolder, boolean oldforge){
		LOGGER.info("Copying Mods");//
		System.out.println("Version:"+id);
		System.out.println("Version:"+gamefolder);
		String versionfolder = ("versions/"+id);
		
		//File jar0 = new File(gamefolder, versionfolder);
				
		String modfolder = (versionfolder+File.separator+"mods");
		File modsfolder = new File(gamefolder, modfolder);
		
		String fmods = "mods/";
		File mods;
		if (oldforge){
		  mods = new File(Util.getWorkingDirectory("minecraft"), fmods);
		}else{
		  mods = new File(gamefolder, fmods);	
		}
		
		deldir(mods);
		mods.delete();
		mods.mkdir();
		
	//	File[]Modslist = modsfolder.listFiles();
		
	//	String dummy;
	//	for(File f : Modslist) {
		//	LOGGER.info("Copying Mods");		    
		  //  dummy = jar1 + f.
		    try {
		    	ModIO.copyFolder(modsfolder, mods);
			} catch (Exception e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			}
	//	}
		
		
	}
	
	public static void addAddons(File gamefolder, Profile profile, boolean oldforge){
		LOGGER.info("Adding Addons");
		System.out.println("Version:"+gamefolder);
		File addons = new File(gamefolder, "addons" + File.separator+profile.getUniID());
		
		File mods;
		if (oldforge){
		  mods = new File(Util.getWorkingDirectory("minecraft"), "mods");
		}else{
		  mods = new File(gamefolder, "mods");	
		}
		
		
		if (!mods.exists()){
		mods.mkdir();}
		
//		File[]Modslist = addons.listFiles();
//		
//		String dummy;
	//	for(File f : Modslist) {
		//	LOGGER.info("Copying Mods");		    
		  //  dummy = jar1 + f.
		    try {
				ModIO.copyFolder(addons, mods);
			} catch (Exception e) {
				// TODO Automatisch generierter Erfassungsblock
				e.printStackTrace();
			}
	//	}
		
		
	}
	
	
//	//�bernimmt libarydownloader
//	public static void addForge(String id, File gamefolder, Launcher launcher){
//		System.out.println("Version:"+id);
//		System.out.println("Version:"+gamefolder);
//		String versionfolder = ("libraries/"+id);
//		
////		File jar0 = new File(gamefolder, versionfolder);
////		String jar1 = jar0 +File.separator+ id+".jar";
////		System.out.println(jar1);
//		
//		String tempfolder = (versionfolder+"/forge");
//		File forgefolder = new File(gamefolder, tempfolder);	
//		
//		File[]Modslist = forgefolder.listFiles();
//		
//		File Forge = Modslist[0];		
//		
//		String[] forgesplit = (Forge.getName().substring(0, Forge.getName().length()-4)).split("-");
//		String forgeversion = forgesplit[3];
//		
//		System.out.println("Forge:" + forgeversion);
//		
//		String libaryonly = "libraries"+File.separator+"net"+File.separator+"minecraftforge"+File.separator+"minecraftforge"+File.separator+forgeversion;
//		
//		File libary = new File(gamefolder, libaryonly);
//		
//		String forgelibaryname = "minecraftforge-" + forgeversion+".jar";
//		File forgelibary = new File(libary, forgelibaryname);
//		
//		if (!forgelibary.exists()){
//				libary.mkdirs();
//				ModIO.copyfile(Forge, forgelibary);
//		}
//		
//		
//		
//	}

	
	public static void deldir(File folder){
		if (folder.isDirectory()){
			File[] files = folder.listFiles();
			if (files != null){
				for (File f : files){
					if(f.isDirectory()){
						deldir(f);
					}else{
						f.delete();
					}
				}
			}
		}
	}

}
