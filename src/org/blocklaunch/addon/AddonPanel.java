package org.blocklaunch.addon;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.html.HTMLDocument;

import net.minecraft.launcher.Launcher;
import org.apache.logging.log4j.LogManager;
import org.blocklaunch.BLUtil;
import org.blocklaunch.start.Util;

import com.kirby0016.ModInstaller.ModIO;

public class AddonPanel extends JSplitPane {
	
	JTextPane infopane = new JTextPane();
	CheckBoxList list = new CheckBoxList(this);
	
	public static File blocklaunchdir = Util.getWorkingDirectory();
	public static File addonsfolder;// = new File(blocklaunchdir, "addons", );
	public static File disabledaddonsfolder;// = new File(addonsfolder, "disabled");
	public static Launcher launcher;
	
	public AddonPanel(Launcher launcher) {
		AddonPanel.launcher = launcher;
//		String id = this.launcher.getProfileManager().getSelectedProfile().getUniID();
//		System.out.println("id "+id);
//		addonsfolder = new File(blocklaunchdir, "addons"+File.separator+id );
//		disabledaddonsfolder = new File(addonsfolder, "disabled");
//		System.out.println(addonsfolder.getAbsolutePath());
		
	//get and declare folders	
		this.refreshFolder();
		
		this.setEnabled(false);
		this.setBorder(new MatteBorder(0, 0, 0, 0, Color.black));
		
		final JPopupMenu listmenu = new JPopupMenu();
		JMenuItem deleteitem = new JMenuItem("Uninstall", new ImageIcon(AddonPanel.class.getResource("/resources/icons/delete.png")));
	    deleteitem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//ask user
//				int result = JOptionPane.showConfirmDialog(AddonPanel.this, "Do you really want to delete this addon?", "Are you sure?", JOptionPane.YES_NO_OPTION);
//				if (result == JOptionPane.YES_OPTION && list.getSelectedValue() != null) {
				deleteListItem();	

			}
		});	    
	    
		listmenu.add(deleteitem);
		
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
		        showPopup(e);
		    }
			
			@Override
		    public void mouseReleased(MouseEvent e) {
		        showPopup(e);
		    }
			
		    private void showPopup(MouseEvent e) {
		    	int index = list.locationToIndex(e.getPoint());
		    	
		        if (e.isPopupTrigger() && index != -1) {
		        	list.setSelectedIndex(index);
		            listmenu.show(e.getComponent(),
		                       e.getX(), e.getY());
		        }
		    }
		});
		//entf taste
		list.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent ke) {
				// TODO Auto-generated method stub
				int code = ke.getKeyCode();
			//	System.out.println("pressed "+String.valueOf(code));
				if (code == KeyEvent.VK_DELETE){
					deleteListItem();
				}
			}
			@Override
			public void keyReleased(KeyEvent arg0) {}
			@Override
			public void keyTyped(KeyEvent arg0) {}	    	
	    });
		
		list.setFont(new Font(list.getFont().getFontName(), Font.PLAIN, 20));
		list.setPreferredSize(new Dimension(200, 10));
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (list.getSelectedIndex() != -1) {
					loadInfo();
				}
			}
		});
		this.setLeftComponent(list);
		
		JScrollPane pnlInfo = new JScrollPane();
		infopane.setEditable(false);
		infopane.setMargin(null);
		infopane.setBackground(Color.WHITE);
		infopane.setContentType("text/html");
		infopane.setText("");
		infopane.addHyperlinkListener(Util.EXTERNAL_HYPERLINK_LISTENER);
		pnlInfo.setBorder(new MatteBorder(0, 0, 0, 0, Color.BLACK));
		pnlInfo.setViewportView(infopane);
		this.setRightComponent(pnlInfo);	
		
		loadList();
		
		DropTarget target = new DropTarget(this, new DropTargetAdapter() {
		    @Override
			public void drop(DropTargetDropEvent dtde) {
				if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
					dtde.acceptDrop(dtde.getDropAction());
					try {
						@SuppressWarnings("unchecked")
						List<File> list = (List<File>) dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
						AddonPanel.this.reloadList();
						for (File file : list) {
							
							if (BLUtil.isAddonFile(file)){								
								ModIO.addAddon(file);								
							}
						}
						//AddonPanel.this.list.updateCheckState();
						loadList(); //reload addon list
					} catch (Exception e) {
						e.printStackTrace();
					}	
				}
			}	
		});
		this.setDropTarget(target);
	}
	
	private void deleteListItem(){
		String filename = ((JCheckBox) list.getSelectedValue()).getText();
		boolean enabled = ((JCheckBox) list.getSelectedValue()).isSelected();
		
		File addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".zip");
		if (!addonfile.exists())
			addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".jar");
		if (!addonfile.exists())
			addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".addon");
		
		//delete addon
		addonfile.delete();
		
		//reload list
		loadList();
	}
	
	private void refreshFolder(){
		//get UniID
		String id = AddonPanel.launcher.getProfileManager().getSelectedProfile().getUniID();
		LogManager.getLogger().info("Refreshing Addonlist for Profile with ID "+String.valueOf(id));
		//declaring addonsfolder
		addonsfolder = new File(blocklaunchdir, "addons"+File.separator+id );
		disabledaddonsfolder = new File(addonsfolder, "disabled");
		//check for existing
		if (!addonsfolder.exists())
			addonsfolder.mkdirs();
		
		if (!disabledaddonsfolder.exists())
			disabledaddonsfolder.mkdirs();
	}
	
	public void reloadList(){
		//LogManager.getLogger().info("Refreshing Addonlist ...");
		this.refreshFolder();
		this.loadList();
	}
	
	public void loadList() {
		
		
		list.setListData(new JCheckBox[] {});
		FileFilter zipjaronly = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".zip") || pathname.getName().endsWith(".jar");						
			}
		};
		
		File[] addons = BLUtil.concatFileArrays(addonsfolder.listFiles(zipjaronly), disabledaddonsfolder.listFiles(zipjaronly));

		Comparator<File> comp = new Comparator<File>() {
			@Override
			public int compare(File file1, File file2) {
				return file1.getName().compareTo(file2.getName());
			}
		};
		Arrays.sort(addons, comp);

		for (File addon : addons) {
			//check for specific file to verify that it's an addon
			try {
				if (!BLUtil.isAddonFile(addon)) continue; //not an addon
				
				JCheckBox box = new JCheckBox(addon.getName().substring(0, addon.getName().length()-4));

				if (!addon.getParentFile().equals(disabledaddonsfolder)) //is NOT disabled
					box.setSelected(true);
	

				//add to list
				addCheckBox(box);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//clear info pane
		infopane.setText("");
		infopane.revalidate();
	}
	
	public void loadInfo() {
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				ZipFile addonzip = null;
				try {
					String filename = ((JCheckBox) list.getSelectedValue()).getText();
					boolean enabled = ((JCheckBox) list.getSelectedValue()).isSelected();
					
					File addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".zip");
					if (!addonfile.exists())
						addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".jar");
					if (!addonfile.exists())
						addonfile = new File(enabled ? addonsfolder : disabledaddonsfolder, filename + ".addon");
					
					addonzip = new ZipFile(addonfile);
					ZipEntry addoninfo = addonzip.getEntry("addon.info");
					
					Properties info = new Properties(); //load infos about addon into Properties
					info.load(addonzip.getInputStream(addoninfo));
					//infopane.setText("<html><body><font color=\"#808080\"><br><br><br><br><br><br><br><center><h1>Loading update news..</h1></center></font></body></html>");
					BufferedReader inforeader = new BufferedReader(new InputStreamReader(AddonPanel.class.getResourceAsStream("addoninfo.html")));
					String htmlinfo = "";
					String line;
					while ((line = inforeader.readLine()) != null) { //replace placeholders
						htmlinfo += line.replace("{title}", info.getProperty("title", "Titel"))
								.replace("{description}", info.getProperty("description", "Description"))
								.replace("{backgroundimg}", AddonPanel.class.getResource("/resources/icons/dirt2.png").toString());
					}
					
					infopane.setText(htmlinfo);
					
					HTMLDocument doc = (HTMLDocument)infopane.getDocument();
					
					String imgurl =  info.getProperty("imgurl");
					System.out.println(imgurl);
					
					if (imgurl != null) {
						htmlinfo = htmlinfo.replace("{titleimg}",imgurl);						
					} else {						
						if (doc.getElement("titleimg") != null)
							doc.removeElement(doc.getElement("titleimg"));						
					}
					String infourl = info.getProperty("url");
					if (infourl != null) {
						htmlinfo = htmlinfo.replace("{infourl}",infourl);
					} else {
						if (doc.getElement("infourl") != null)
							doc.removeElement(doc.getElement("infourl"));
					}
					
//					String background = info.getProperty("background");
//					if (infourl != null) {
//						htmlinfo = htmlinfo.replace("{backgroundimg}",background);
//					} else {
//						if (doc.getElement("backgroundimg") != null)
//							doc.removeElement(doc.getElement("backgroundimg"));
//					}
					
					infopane.setText(htmlinfo);
					infopane.revalidate();
					infopane.repaint();
				} catch (Exception localException) {
					localException.printStackTrace();
					infopane.setText("<html><body><font color=\"#808080\"><br><br><br><br><br><br><br><center><h1>Failed to load info</h1><br>"
							+ localException.toString()
							+ "</center></font></body></html>");
				} finally {
					if (addonzip != null)
						try {
							addonzip.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			}
		});
	}
	
	private void addCheckBox(final JCheckBox box) {
		list.addCheckbox(box);
	}
	
	public void updateEnabledList(int listindex) {
		
		JCheckBox item = (JCheckBox) list.getModel().getElementAt(listindex);
		boolean enableme = item.isSelected();
		
		//enable me = from disabled to addons folder; don't enable me = get from addons folder and put into disabled folder
		File addonfile = new File(enableme ? disabledaddonsfolder : addonsfolder, item.getText() + ".zip");
		if (!addonfile.exists())
			addonfile = new File(enableme ? disabledaddonsfolder : addonsfolder, item.getText() + ".jar");
		if (!addonfile.exists())
			addonfile = new File(enableme ? disabledaddonsfolder : addonsfolder, item.getText() + ".addon");
		
		//didn't work always
		//addonfile.renameTo(new File(enableme ? addonsfolder : disabledaddonsfolder, addonfile.getName()));
		ModIO.copyfile(addonfile, new File(enableme ? addonsfolder : disabledaddonsfolder, addonfile.getName()));
		addonfile.delete();
	}
	
	private static final long serialVersionUID = 1L;
}
