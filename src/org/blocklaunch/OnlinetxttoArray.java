package org.blocklaunch;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Vector;

public class OnlinetxttoArray {
	
	public static Vector<String> getBase(String fileurl){
        Vector<String> text = new Vector<String>(); // mit arraylist austauschen f�r gr��ere dateien
        String dummy = null;

       try
       {            
          URL url = new URL(fileurl);
        //  URLConnection connect = url.openConnection();
          InputStream is = url.openStream();
        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
          BufferedReader br = new BufferedReader(new InputStreamReader(is));

          
          while ((dummy = br.readLine()) != null){
        	  if (!(dummy.startsWith("#"))&&!dummy.equals("")&&(!(dummy.startsWith("[")))){ 
             text.add(dummy);}
          }
       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       return text;
    }
	
	public static Vector<String> getMod(String fileurl){
        Vector<String> text = new Vector<String>(); // mit arraylist austauschen f�r gr��ere dateien
        String dummy = null;

       try
       {            
          URL url = new URL(fileurl);
        //  URLConnection connect = url.openConnection();
          InputStream is = url.openStream();
        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
          BufferedReader br = new BufferedReader(new InputStreamReader(is));

          
          while ((dummy = br.readLine()) != null){
        	  if ((dummy.startsWith("[Mod]"))&&!dummy.equals("")){
        		  int pos = dummy.length();
             text.add(dummy.substring(5, pos));
             System.out.println(dummy.substring(5));
        	  }
          }
       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       return text;
    }
	
	public static Vector<String> getOldForgelib(String fileurl){
        Vector<String> text = new Vector<String>(); // mit arraylist austauschen f�r gr��ere dateien
        String dummy = null;

       try
       {            
          URL url = new URL(fileurl);
        //  URLConnection connect = url.openConnection();
          InputStream is = url.openStream();
        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
          BufferedReader br = new BufferedReader(new InputStreamReader(is));

          
          while ((dummy = br.readLine()) != null){
        	  if ((dummy.startsWith("[Lib]"))&&!dummy.equals("")){
        		  int pos = dummy.length();
             text.add(dummy.substring(5, pos));
             System.out.println(dummy.substring(5));
        	  }
          }
       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       return text;
    }

	public static Vector<String> getForge(String fileurl){
        Vector<String> text = new Vector<String>(); // mit arraylist austauschen f�r gr��ere dateien
        String dummy = null;

       try
       {            
          URL url = new URL(fileurl);
        //  URLConnection connect = url.openConnection();
          InputStream is = url.openStream();
        //  BufferedReader br = new BufferedReader( new FileReader(is.toString()) );
          BufferedReader br = new BufferedReader(new InputStreamReader(is));

          
          while ((dummy = br.readLine()) != null){
        	  if ((dummy.startsWith("[Forge]"))&&!dummy.equals("")){
        		  int pos = dummy.length();
             text.add(dummy.substring(7, pos));
          //   System.out.println(dummy.substring(7, pos));
        	  }
          }
       }
       catch(Exception ex)
       {
          ex.printStackTrace();
       }

       return text;
    }

	
	public static List readFile(BufferedReader is) throws IOException{
		BufferedReader br = null;
		try{
		      //br = new BufferedReader( new InputStreamReader(is) );
		      StringBuffer text = new StringBuffer();
		      String line;
		      List texter = null;
		      while ( (line=br.readLine()) !=null ){
		    	  if (line.startsWith("#")) continue;
		    	 texter.add(line);}
		    	//  text.append( line+"\n");}}
		      

		      return texter;
		}finally{
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }
		}
		   
	}

}
