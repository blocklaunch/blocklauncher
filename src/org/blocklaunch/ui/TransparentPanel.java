package org.blocklaunch.ui;

import java.awt.Graphics;

import javax.swing.JPanel;

public class TransparentPanel extends JPanel {
	
	public TransparentPanel(boolean bool){
		super(bool);
	}
	
	public TransparentPanel(){
		super();
	}
	
	@Override
    protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, getSize().width, getSize().height);
    }

}
