package org.blocklaunch.serverinstaller;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.profile.Profile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.blocklaunch.BLUtil;
import org.blocklaunch.Link;
import org.blocklaunch.OnlinetxttoArray;
import org.blocklaunch.serverinstaller.serverdownloadlistener;

import com.kirby0016.ModInstaller.ModIO;
import com.mojang.launcher.updater.download.DownloadJob;
import com.mojang.launcher.updater.download.DownloadListener;
import com.mojang.launcher.versions.Version;

public class serverinstall 
	//implements DownloadListener
							{
	
	private static File installdir;
//	private static boolean isDownloading = false;
	private static boolean oldforgeinstall = false;
	
	static File Forgefile;
	private static String jarFile;
	
	static Logger LOGGER = LogManager.getLogger();
	
	private static Launcher launcher;
	
	
	//url s3.amazonaws.com/Minecraft.Download/versions/1.4.2/minecraft_server.1.4.2.jar
    //min version = 1.3.1 ansonsten assets.minecarft.net
	
	//-1. Vezeichnis abfragen check
	//0. downloadjob erstellen ckeck
	//1. serverjar > downlaodjob check
	//2. mods get + zu downlaodjob check
	//4. forge > downloadjob check
	//5. job ausf�hren ceck
	//6. forge installieren (post 1.6 > pr�fen wie (libaryfilesinstall) check
	//6a. addons r�berkopieren check
	//7. startskript erstellen check - nicht fehlerfrei
	//8. serverordner �ffnen (server starten?) check
	
	public static void start(int RAM, boolean backup, Version version, String javaarguments,
			Profile profile, Launcher launchers, boolean start){
		launcher = launchers;
		if (selectfolder(new JFrame())) {
			

				DownloadJob libaries = new DownloadJob("Version & Mods", true, (DownloadListener) new serverdownloadlistener());
				try {
					boolean forge= false;
					if(serverinstall.setdownloads(libaries, version, profile)){
					serverinstall.installforge();
					forge = true;}
					createbackup(forge, backup, javaarguments, RAM, start);
					Serverinstalldialog.getProgress().setValue(100);
					LOGGER.info("Finished");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		}		
	}
	
	
	
	private static void createbackup(boolean forge, boolean backup, String javaarguments,
			int ram, boolean start) throws Exception {
		// TODO Auto-generated method stub
		File batchfile = new File(installdir, "start.bat");
		batchfile.createNewFile();
		FileOutputStream fos = new FileOutputStream(batchfile);
		DataOutputStream dos = new DataOutputStream(fos);
//		try{
		 String serverfile_string;
		 if(forge){serverfile_string = "minecraftforge.jar";}else{serverfile_string = "minecraft_server.jar";};
		 File serverfile = new File(installdir, serverfile_string);
		 dos.writeBytes("java -Xmx"+ram+"M "+javaarguments+" -jar "+serverfile.getAbsolutePath()+" \n");
		 System.out.println("java -Xmx"+ram+"M "+javaarguments+" -jar "+serverfile.getAbsolutePath());
		 if (backup){			
		 dos.writeBytes("echo start backup \n");
		 dos.writeBytes("For /F \"tokens=* skip=3\" %%a IN (server.properties) DO echo %%a>>TEMP.txt \n");
		 dos.writeBytes("set /p WORLD=<TEMP.txt \n");
		 dos.writeBytes("del TEMP.txt \n");
		 dos.writeBytes("for /f \"tokens=2 delims== \" %%a in (\"%WORLD%\") do set WORLD=%%a \n");
		 dos.writeBytes("FOR /F \"tokens=1-3 delims=/.- \" %%A IN ('date /T') DO (SET TT=%%A&SET MM=%%B&SET JJJJ=%%C) \n");
		 dos.writeBytes("SET newDate=%JJJJ%-%MM%-%TT% \n");
		 dos.writeBytes("FOR /F \"tokens=1-4 delims=:, \" %%A IN ('time /T') DO (SET hh=%%A& SET mm=%%B) \n");
		 dos.writeBytes("SET time=%hh%-%mm% \n");
		 dos.writeBytes("SET dir=%newDate%-%time%-%WORLD% \n");
		 dos.writeBytes("if exist %WORLD% md BACKUP"+File.separator+"%dir% \n");
		 dos.writeBytes("SET COUNTER=0 \n");
		 dos.writeBytes(":loop \n");
		 dos.writeBytes("SET /a COUNTER=%COUNTER%+1 \n");
		 dos.writeBytes("XCOPY \"%WORLD%\" "+installdir.getAbsolutePath()+File.separator+"BACKUP"+File.separator+"%dir%"+File.separator+" /Y /E /S /Q \n");
		 dos.writeBytes("exit \n");
		 dos.writeBytes("goto loop \n");
		 dos.writeBytes("echo backup finished \n");
		 }
//		 dos.writeBytes("echo starte backup\n");
//		 writer.print(s);
//		}finally{
		 fos.close();
		 dos.close();
		 LOGGER.info("Created Starting batch");
		 if (start){
		 Runtime.getRuntime().exec("cmd.exe /c start "+batchfile.toString());
//		 }
		}
		
		
	}



	public static boolean setdownloads(DownloadJob libaries, Version version, Profile profile) throws IOException{	
		boolean blmode = false;
		if (BLUtil.isBL(version.getId())){
			blmode = true;
		}	
		//1. jarfileDL
		String DLurl = "https://s3.amazonaws.com/Minecraft.Download/";
		
		String jarFileDL;
		
		if (blmode){			
			String[] VersionParts = version.getId().split(Pattern.quote("_"));
			jarFileDL = "versions/" + VersionParts[2] + "/minecraft_server." + VersionParts[2] + ".jar";
			jarFile = "minecraft_server." + VersionParts[2] + ".jar";
			//downlaoding minecarftversion for old forge
			if((version.getId().endsWith("1.5.2"))||(version.getId().endsWith("1.5.1"))){
				oldforgeinstall = true;	
				jarFile = "minecraft_server.jar";
			}
		}else{
			jarFileDL = "versions/" + version.getId() + "/minecraft_server." + version.getId() + ".jar";
			jarFile = "minecraft_server.jar";
			//serverinstall.download(new URL(DLurl + jarFileDL), new File(installdir, jarFile));			
		}
		Serverinstalldialog.getProgress().setValue(12);
		
		
		serverinstall.download(new URL(DLurl + jarFileDL), new File(installdir, jarFile));
		
		Serverinstalldialog.getProgress().setValue(20);
		
		//serverjar
	//	serverinstall.download(new URL(DLurl + jarFileDL), new File(installdir, jarFile));
	//	libaries.addDownloadables(new Downloadable[]{new EtagDownloadable(Launcher.getInstance().getProxy(), new URL(DLurl + jarFileDL), new File(installdir, jarFile), true)});

		if(blmode){
			//Forge
			
			
			String modsurl = (Link.DL_URL+"versions/"+version.getId()+"/mods.txt");
			URL Forge;
			//File Forgefile;
			if (oldforgeinstall) {
			//pre 1.6 forge via zippen		
		     	//Get Basemods
		     	Vector<String> base = OnlinetxttoArray.getBase(modsurl); 
		     	Forge = new URL(base.get(0));
		     	String forgestr = "minecraftforge.jar";
		   System.out.println(forgestr);
		   		Forgefile = new File(installdir, forgestr);
		   		//old libs
		   		Vector<String> libs = OnlinetxttoArray.getOldForgelib(modsurl);
		   		File Libfolder = new File(installdir, "lib");
		   		if(!Libfolder.exists()){Libfolder.mkdir();}
		   		for(int i=0; i<libs.size(); i++){
		   			String libDL=libs.get(i);
		   			String name = new File(libDL).getName(); 
		   			File lib = new File(Libfolder, name);
		   			serverinstall.download(new URL(libDL), lib);
		   		}
		   		
			}else{
				//get Forge
		     	Vector<String> forgelist = OnlinetxttoArray.getForge(modsurl);
		     	//rename downloadable to installer
	     			String basenameold = FilenameUtils.getBaseName(forgelist.get(0));
	     			String[] FP = basenameold.split(Pattern.quote("-"));
	     			
	     			if (version.getId().endsWith("1.6.4")){
	     				FP[1] = "installer";	     				
	     			}else{
	     				FP[3] = "installer";
	     			}
	     			String basenamenew = FP[0]+"-"+FP[1]+"-"+FP[2]+"-"+FP[3];	
	     			String[] FUP = forgelist.get(0).split("/");
		     		FUP[FUP.length-1]=basenamenew + ".jar";
		     		String forgedl = "http:/";
		     		for (int i=2; i<FUP.length; i++) {
		     			forgedl = forgedl + "/" + FUP[i];
		     		}
//		     System.out.println(forgedl);		
		     	Forge = new URL(forgedl);
		     	
		     	String forgestr = basenamenew+"."+FilenameUtils.getExtension(forgelist.get(0));
		     	Forgefile = new File(installdir, forgestr);		     	
		     	
		     	File Forgefile2 = new File(installdir, "minecraftforge.jar");
		     	serverinstall.download(new URL(forgelist.get(0)), Forgefile2);
			}
			serverinstall.download(Forge, Forgefile);
			Serverinstalldialog.getProgress().setValue(21);
			//download forge
		//	libaries.addDownloadables(new Downloadable[]{new EtagDownloadable(Launcher.getInstance().getProxy(), Forge, Forgefile, true)});
			
			Serverinstalldialog.getProgress().setValue(27);
			String name;
	     	String ModURL;
	     	File ModFile;
	     	
	     	Vector<String> mods = OnlinetxttoArray.getMod(modsurl);
	     	
	     	File modfolder = new File(installdir, "mods");
	     	if (!modfolder.exists()){
	     		modfolder.mkdir();
	     	}
	     	//addons
			String uniid = profile.getUniID();
			File gameDir = profile.getGameDir() == null ? launcher.getLauncher().getWorkingDirectory() : profile.getGameDir();
			 
			File addonsdir = new File(new File(gameDir, "addons"), uniid);
			ModIO.copyFolder(addonsdir, modfolder);
			Serverinstalldialog.getProgress().setValue(30);
	     	
			//mods
			for (int i = 0; (i < (mods.size())); i++){
	     		
	     		ModURL = mods.get(i);
	     		
	     		name = new File(ModURL).getName();
	     		
	     		ModFile = new File(modfolder, name); 
	     		//DL	     		
	     		serverinstall.download(new URL(ModURL), ModFile);
	     		Serverinstalldialog.getProgress().setValue(30+(i*(20/mods.size())));
	     //		libaries.addDownloadables(new Downloadable[]{new EtagDownloadable(Launcher.getInstance().getProxy(), new URL(ModURL), ModFile, true)});
	     		
			}			
		}
		
		
		
//		isDownloading = false;
		LOGGER.info("All Downloads finished\n");
	//	libaries.startDownloading(Launcher.getInstance().getDownloaderExecutorService());
		if(blmode){
			return true;			
		}else{
			Serverinstalldialog.getProgress().setValue(90);
			return false;
		}
		
	}
	
	public static void download(URL source, File destination) throws IOException{
		LOGGER.info("Start Downloading " +source);
	//	System.out.println(source.getProtocol()+"  "+source.getHost() + "  "+ source.getFile());
		FileUtils.copyURLToFile(source, destination);
		LOGGER.info("Finished Downloading " + destination);
	}
	
	//-1. 
	public static boolean selectfolder(JFrame frame){
		
		JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(1);
        fileChooser.setApproveButtonText("Select");
        fileChooser.setDialogTitle("Choose your Server installation directory : (Cancelling will cancel creating)");
        int ret = fileChooser.showOpenDialog(frame);
        if(ret == 1){return false;}
        else{
        	installdir = fileChooser.getSelectedFile();
        	LOGGER.info("Server will installed in "+installdir.getAbsolutePath());
        	if (!installdir.exists()){
        		installdir.mkdir();
        	}
        	return  true;
        }
        
	}
	
	//installforge
	private static void installforge(){
		//pre 1.6 via zippen
		//post via silentinstaller
		LOGGER.info("start installing forge");
		if (oldforgeinstall){
			try {
				ModIO.addmod(serverinstall.Forgefile.getAbsolutePath(), new File(installdir, "minecraft_server.jar"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				(new InstallForgeServer()).install(Forgefile.getParent(), Forgefile);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				LOGGER.info("Forgeinstall finished");				
				//json.delete();
			}			
		}
		Serverinstalldialog.getProgress().setValue(75);
		
	}
	
//	@Override
//	public void onDownloadJobFinished(DownloadJob arg0) {
//		// TODO Auto-generated method stub
//		LOGGER.info("All Downloads finished");
//		this.isDownloading = false;
//		this.installforge();
//		
//		
//	}
//	@Override
//	public void onDownloadJobProgressChanged(DownloadJob arg0) {
//		// TODO Auto-generated method stub
//		//updateprogressbar?
//		System.out.println(String.valueOf(arg0.getProgress()));
//	}
}
