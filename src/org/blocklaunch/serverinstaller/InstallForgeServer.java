package org.blocklaunch.serverinstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.minecraft.launcher.LauncherConstants;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class InstallForgeServer {
	//http://repo1.maven.org/maven2/org/scala-lang/scala-library/2.10.2/scala-library-2.10.2.jar
	private static final String altforgemaven = "http://repo.maven.org/maven2/";
	private static final String dlURL = LauncherConstants.URL_LIBRARY_BASE;
	
	public void install(String string0, File Forgeinstaller) throws Exception{
		File string = new File(string0);
		File librariesdir = new File(string, "libraries");
		if(!string.exists()){ string.mkdirs();}
		librariesdir.mkdir();
		
		//final Gson gson = new Gson();
		
		//InputStream installProfile = null;
		
		File jsonfile = new File(string, "install_profile.json");
		try{
			extractjson(Forgeinstaller.getAbsolutePath(), jsonfile);
		
			List<File> libaries = new ArrayList<File>();
			List<String> libariesDL = new ArrayList<String>();
		
			JsonElement json = (new JsonParser()).parse(FileUtils.readFileToString(jsonfile));

			JsonObject jobject = json.getAsJsonObject();
			jobject = jobject.getAsJsonObject("versionInfo");
			JsonArray jarray = jobject.getAsJsonArray("libraries");
			for (int i=0; i<jarray.size(); i++){
				JsonObject jspezobject = jarray.get(i).getAsJsonObject();			
				if(jspezobject.has("serverreq")){
					if(jspezobject.get("serverreq").getAsBoolean()){
						if(jspezobject.get("url") == null){
							String name = getlibarydl(jspezobject.get("name").toString());
							libaries.add(new File(librariesdir, name));
							libariesDL.add(dlURL+name);
						}else{
							String name = getlibarydl(jspezobject.get("name").toString());
							libaries.add(new File(librariesdir, name));
							libariesDL.add(getdl(jspezobject.get("url").toString(),name));
						}
					}
				}
			}
//		System.out.println("5 "+libaries.toString());
//		System.out.println("51 "+libariesDL.toString());
//		
			//dl
			for(int i=0; i<libariesDL.size(); i++){
				serverinstall.download(new URL(libariesDL.get(i)), libaries.get(i));
			}
		}finally{
			jsonfile.delete();
			Forgeinstaller.delete();
		}
		
	}
	
	private String getdl(String url, String name){
		String url1 = url.substring(1, url.length()-1);
		System.out.println(url1+name);
		if(name.contains("scala")){
			url1 = altforgemaven;
		}
		return url1+name;
	}
	
	private String getlibarydl(String name){
		//"org.ow2.asm:asm-all:4.1"
		name = name.substring(1, name.length()-1);
		String[] namepart = name.split(":"); 
		namepart[0] = namepart[0].replace('.', '/');
		String jarname = namepart[1] + '-' + namepart[2] + ".jar";
		String pathname = namepart[0] + '/' + namepart[1] + '/' + namepart[2] + '/' + jarname;
		return pathname;		
	}
	
	public void extractjson(String zip, File dest) throws Exception{
		//String destinationname = "d:\\";
	    byte[] buf = new byte[1024];
	    ZipInputStream zipinputstream = null;
	    ZipEntry zipentry;
	    zipinputstream = new ZipInputStream(new FileInputStream(zip));
		zipentry = zipinputstream.getNextEntry();
	    while (zipentry != null) {
	      String entryName = zipentry.getName();
	      if (entryName.endsWith(".json")){
	      FileOutputStream fileoutputstream;
//	      File newFile = new File(entryName);
//	      String directory = newFile.getParent();

//	      if (directory == null) {
//	        if (newFile.isDirectory())
//	          break;
//	      }
	      fileoutputstream = new FileOutputStream(dest);
	      int n;
	      while ((n = zipinputstream.read(buf, 0, 1024)) > -1){
	        fileoutputstream.write(buf, 0, n);
	      }
	      fileoutputstream.close();
	      zipinputstream.closeEntry();
	      }
	      zipentry = zipinputstream.getNextEntry();
	    }
	    zipinputstream.close();
	}

}
