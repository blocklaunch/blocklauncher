package org.blocklaunch.serverinstaller;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JCheckBox;

import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.game.MinecraftReleaseType;
import net.minecraft.launcher.profile.Profile;

import org.blocklaunch.BLUtil;

import com.mojang.launcher.updater.VersionSyncInfo;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Serverinstalldialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtxmsm;
	
	//JComboBox versionList = new JComboBox();
	JSpinner spinner = new JSpinner();
	JCheckBox chckbxBackupWorldWhen;
	JCheckBox chckbxStartserver;
	JLabel lblprofile = new JLabel("???");
	JLabel lblversion = new JLabel("???");
	JButton okButton = new JButton("Create and Start Server");

	static JProgressBar progress;
	
	private Launcher launcher;
	//private String version;
	private String latestid = null;
	private VersionSyncInfo syncinfo = null;
	private String profile = "?";
	private boolean alreadylaunched = false;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		try {
//			Serverinstalldialog dialog = new Serverinstalldialog();
//			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
// frame = ((SwingUserInterface) minecraftLauncher.getUserInterface()).getFrame();

	/**
	 * Create the dialog.
	 */
	public Serverinstalldialog(Launcher launcher) {
		this.launcher = launcher;	
		try
	     {
	       InputStream in = Launcher.class.getResourceAsStream("/resources/icons/AddonCarton.png");
	       if (in != null)
	         this.setIconImage(ImageIO.read(in));
	     }
	     catch (IOException localIOException){}
	    
	  //  createInterface();
	  
	}
		
	private void createInterface(){
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Create Server - Settings");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblprofilename = new JLabel("Profile:");
		//	lblversionname.setVerticalAlignment(JLabel.RIGHT);
			GridBagConstraints gbc_lblprofilename = new GridBagConstraints();
			gbc_lblprofilename.insets = new Insets(0, 0, 5, 5);
			gbc_lblprofilename.gridx = 0;
			gbc_lblprofilename.gridy = 0;
			
			contentPanel.add(lblprofilename, gbc_lblprofilename);
			}
			{	
				
				
				lblprofile = new JLabel(this.profile);
				this.addversionlabel();
				lblprofile.setVerticalAlignment(JLabel.CENTER);
				GridBagConstraints gbc_lblprofile = new GridBagConstraints();
				gbc_lblprofile.insets = new Insets(0, 0, 5, 5);
				gbc_lblprofile.gridx = 1;
				gbc_lblprofile.gridy = 0;
				
				contentPanel.add(lblprofile, gbc_lblprofile);
			}
		{
		JLabel lblversionname = new JLabel("Version:");
	//	lblversionname.setVerticalAlignment(JLabel.RIGHT);
		GridBagConstraints gbc_lblversionname = new GridBagConstraints();
		gbc_lblversionname.insets = new Insets(0, 0, 5, 5);
		gbc_lblversionname.gridx = 0;
		gbc_lblversionname.gridy = 1;
		
		contentPanel.add(lblversionname, gbc_lblversionname);
		}
		{	
			
			
			lblversion.setText(BLUtil.renameVersions(this.syncinfo.getLatestVersion()));
			this.addversionlabel();
			lblversion.setVerticalAlignment(JLabel.CENTER);
			GridBagConstraints gbc_lblversion = new GridBagConstraints();
			gbc_lblversion.insets = new Insets(0, 0, 5, 5);
			gbc_lblversion.gridx = 1;
			gbc_lblversion.gridy = 1;
			
			contentPanel.add(lblversion, gbc_lblversion);
		}
		
		{
			JLabel lblMaximumRam = new JLabel("Maximum RAM:");
			//lblMaximumRam.setVerticalAlignment(JLabel.RIGHT);
			GridBagConstraints gbc_lblMaximumRam = new GridBagConstraints();
			gbc_lblMaximumRam.insets = new Insets(0, 0, 5, 5);
			gbc_lblMaximumRam.gridx = 0;
			gbc_lblMaximumRam.gridy = 2;
			contentPanel.add(lblMaximumRam, gbc_lblMaximumRam);
		}
		{
			int maxram;
			if (!System.getProperty("os.arch").equals("x86")) { //64bit
		    maxram = 8196;}
		    else{//32bit
		    	maxram = 4096;
		    }			
			spinner.setModel(new SpinnerNumberModel(1024, 1024, maxram, 256));
			GridBagConstraints gbc_spinner = new GridBagConstraints();
			gbc_spinner.fill = GridBagConstraints.HORIZONTAL;
			gbc_spinner.insets = new Insets(0, 0, 5, 5);
			gbc_spinner.gridx = 1;
			gbc_spinner.gridy = 2;
			contentPanel.add(spinner, gbc_spinner);
		}
		{
			JLabel lblMb = new JLabel("MB");
			GridBagConstraints gbc_lblMb = new GridBagConstraints();
			gbc_lblMb.insets = new Insets(0, 0, 5, 0);
			gbc_lblMb.gridx = 2;
			gbc_lblMb.gridy = 2;
			contentPanel.add(lblMb, gbc_lblMb);
		}
		{
			JLabel lblJavaArguments = new JLabel("Java Arguments:");
			//lblJavaArguments.setVerticalAlignment(JLabel.RIGHT);
			GridBagConstraints gbc_lblJavaArguments = new GridBagConstraints();
			gbc_lblJavaArguments.insets = new Insets(0, 0, 5, 5);
			gbc_lblJavaArguments.anchor = GridBagConstraints.EAST;
			gbc_lblJavaArguments.gridx = 0;
			gbc_lblJavaArguments.gridy = 3;
			contentPanel.add(lblJavaArguments, gbc_lblJavaArguments);
		}
		{
			txtxmsm = new JTextField();
			txtxmsm.setText("-Xms512M");
			GridBagConstraints gbc_txtxmsm = new GridBagConstraints();
			gbc_txtxmsm.insets = new Insets(0, 0, 5, 0);
			gbc_txtxmsm.gridwidth = 2;
			gbc_txtxmsm.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtxmsm.gridx = 1;
			gbc_txtxmsm.gridy = 3;
			contentPanel.add(txtxmsm, gbc_txtxmsm);
			txtxmsm.setColumns(10);
		}
		{
			chckbxBackupWorldWhen = new JCheckBox("Backup world when close Server ");
//			chckbxBackupWorldWhen.setSelected(true);
			//tempoary
			chckbxBackupWorldWhen.setSelected(false);
			chckbxBackupWorldWhen.setEnabled(false);
			GridBagConstraints gbc_chckbxBackupWorldWhen = new GridBagConstraints();
			gbc_chckbxBackupWorldWhen.insets = new Insets(0, 0, 0, 5);
			gbc_chckbxBackupWorldWhen.gridwidth = 2;
			gbc_chckbxBackupWorldWhen.gridx = 1;
			gbc_chckbxBackupWorldWhen.gridy = 4;
			contentPanel.add(chckbxBackupWorldWhen, gbc_chckbxBackupWorldWhen);
		}
		{
			chckbxStartserver = new JCheckBox("Start Server after finishing");
			chckbxStartserver.setSelected(true);
			chckbxStartserver.addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent e)
		         {if(!chckbxStartserver.isSelected()){
		        	 okButton.setText("Create Server");
					}else{
					 okButton.setText("Create and Start Server");
					}					
		         }
			});
			GridBagConstraints gbc_Startserver = new GridBagConstraints();
			gbc_Startserver.insets = new Insets(0, 0, 0, 5);
			gbc_Startserver.gridwidth = 2;
			gbc_Startserver.gridx = 1;
			gbc_Startserver.gridy = 5;
			contentPanel.add(chckbxStartserver, gbc_Startserver);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				//JButton okButton = new JButton("Create Server");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {	
						progress.setVisible(true);
						try{
						progress.setValue(0);
						Profile profile = launcher.getProfileManager().getSelectedProfile();
						serverinstall.start(Integer.parseInt(spinner.getValue().toString()), 
								chckbxBackupWorldWhen.isSelected(), syncinfo.getLatestVersion(), 
								txtxmsm.getText(), profile, launcher, chckbxStartserver.isSelected());
						}finally{
						progress.setVisible(false);
						Serverinstalldialog.this.dispose();
						}
						
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				{
					progress = new JProgressBar();
					progress.setMaximum(100);
					progress.setVisible(false);
						//getContentPane().add(progress, BorderLayout.SOUTH);
					buttonPane.add(progress);					
				}
			}
//			{
//				JButton cancelButton = new JButton("Cancel");
//				cancelButton.setActionCommand("Cancel");
//				buttonPane.add(cancelButton);
//			}
		}
	}
	
	public static JProgressBar getProgress(){
		return Serverinstalldialog.progress;
	}
	
	private void addversionlabel(){
		{	
			lblversion.setText(BLUtil.renameVersions(this.syncinfo.getLatestVersion()));
			lblprofile.setText(launcher.getProfileManager().getSelectedProfile().getName());
			
//			lblversion.setVerticalAlignment(JLabel.CENTER);
//			GridBagConstraints gbc_lblversion = new GridBagConstraints();
//			gbc_lblversion.insets = new Insets(0, 0, 5, 5);
//			gbc_lblversion.gridx = 1;
//			gbc_lblversion.gridy = 0;
//			
//			contentPanel.add(lblversion, gbc_lblversion);
		}
	}
	
	public void refreshserverversion(){
		boolean launchdialog = false;
		latestid = launcher.getProfileManager().getSelectedProfile().getLastVersionId();
		if(latestid != null){
		syncinfo = launcher.getLauncher().getVersionManager().getVersionSyncInfo(latestid);
		//this.version = launcher.getVersionManager().getLatestCompleteVersion(syncinfo).getId();
		this.profile = launcher.getProfileManager().getSelectedProfile().getName();
		
		}
		//no alpha/beta/1.0 ect.
		Date mintime = null;
		try {
			mintime = new SimpleDateFormat("yyyy-MM-dd").parse("2012-03-29");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (syncinfo.getLatestVersion().getReleaseTime().compareTo(mintime) <= 0){
			if ((syncinfo.getLatestVersion().getType() == MinecraftReleaseType.OLD_ALPHA)||(syncinfo.getLatestVersion().getType() == MinecraftReleaseType.OLD_BETA)){
				JOptionPane.showMessageDialog(null, "Serverinstaller doesn't support Minecarft Alpha or Beta");}
			else{
				JOptionPane.showMessageDialog(null, "Serverinstaller doesn't support Minecarft younger than Version 1.2.5");	
				}
			
		}else{
			if (!alreadylaunched){
			this.alreadylaunched = true;
			this.createInterface();
			this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);	
			//this.setAlwaysOnTop(true);
			this.setModal(true);
			}
			this.addversionlabel();
			this.lblversion.revalidate();
			launchdialog = true;
		}
//			if ((syncinfo == null) || (syncinfo.getLatestVersion() == null)) {
		//}
		this.setVisible(launchdialog);
	}

}
