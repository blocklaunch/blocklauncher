package org.blocklaunch.mcserverapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ServiceInformation {

    private URL url;
    private JsonObject jsonObject;
    
    private String service = null;

    private String location_name = null,
            location_city = null,
            location_country = null,
            status = null,
            uptime_percent = null,
            uptime_upMinutes = null,
            uptime_downMinutes = null,
            uptime_currentDowntime = null,
    		//apicode
    		apiCode = "VSdXyhPnA4pY";
    
    public ServiceInformation(String service) throws MalformedURLException {
        this.url = new URL("http://mcstatus.sweetcode.de/publicAPI.php?code=" + this.apiCode + "&request=" + service);

        this.service = service;
    }
    
    public ServiceInformation(String service, boolean fetchData) throws IOException {
    	this(service);
    	
    	if(fetchData) {
    		this.fetchData();
    	}
    }
    
    public final String getService() {
    	return service;
    }
    
    public final JsonObject getObject() {
    	return jsonObject;
    }
    
    public final String getStatus() {
    	return status;
    }
    
    public final String getLocationName() {
    	return location_name;
    }
    
    public final String getCity() {
    	return location_city;
    }
    
    public final String getCountry() {
    	return location_country;
    }
    
    public final String getUptimePercent() {
    	return uptime_percent;
    }
    
    public final String getUptimesUpMinutes() {
    	return uptime_upMinutes;
    }
    
    public final String getUptimeDownMinutes() {
    	return uptime_downMinutes;
    }
    
    public final String getCurrentDowntime() {
    	return uptime_currentDowntime;
    }
    
    
    public void fetchData() throws IOException {
        StringBuilder jsonString = new StringBuilder();
        InputStream inputStream = url.openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            jsonString.append(line);
        }

        inputStream.close();
        bufferedReader.close();
        //--------
        
        //set the jsonObject
        JsonObject jsonObject = (JsonObject) ((JsonObject) (new JsonParser()).parse(jsonString.toString())).get(service);
        this.jsonObject = jsonObject;
        //--------
        
        //set location
        JsonObject location = (JsonObject) jsonObject.get("location");

        this.location_name = (String) location.get("name").getAsString();
        this.location_city = (String) location.get("city").getAsString();
        this.location_country = (String) location.get("country").getAsString();
        //--------------

        //set status
        this.status = (String) jsonObject.get("status").getAsString();
        //--------------

        //set uptime
        JsonObject uptime = (JsonObject) jsonObject.get("uptime");

        this.uptime_percent = (String) uptime.get("percent").getAsString();
        this.uptime_upMinutes = (String) uptime.get("upMinutes").getAsString();
        this.uptime_downMinutes = (String) uptime.get("downMinutes").getAsString();
        this.uptime_currentDowntime = (String) uptime.get("currentDowntime").getAsString();
        //--------------
    }

}

