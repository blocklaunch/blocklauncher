package com.kirby0016.ModInstaller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ModpartConfigFile extends Modpart {
	public static boolean doIO(ZipEntry zipEntry, ZipFile mod) throws Exception {
		File configfolder = new File(Minecraft.getAppDir("blocklaunch"), "config");
		
		File file = new File(configfolder, zipEntry.getName().replace("/",
				File.separator));
		
		//copy config file to config folder
		InputStream is = mod.getInputStream(zipEntry);
		FileOutputStream fos = new FileOutputStream(file);
		ModIO.copy(is, fos);
		is.close();
		fos.close();
		
		return true;
	}
	
	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry) {
		return zipEntry.getName().endsWith(".cfg") && zipEntry.getName().split("/").length == 1;
	}

}
