package com.kirby0016.ModInstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.blocklaunch.addon.AddonPanel;
import org.blocklaunch.start.Util;

public class ModIO {

	// 4MB buffer
	private static final byte[] BUFFER = new byte[4096 * 1024];

	public static void copyfile(File srcFile, File dstFile) {
		InputStream in = null;
		OutputStream out = null;
		try {
			if (!dstFile.getParentFile().exists()) {
				dstFile.getParentFile().mkdirs();
			}
			if (!dstFile.exists()) {
				dstFile.createNewFile();
			}
			in = new FileInputStream(srcFile);

			out = new FileOutputStream(dstFile);

			byte[] buf = new byte[1024];
			int len;

			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
				try {
					if (in != null)
						in.close();
					
					if (out != null)
						out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			
		}
	}

	public static void copyFolder(File src, File dest) {

		if (src.isDirectory()) {

			// if directory not exists, create it
			if (!dest.exists()) {
				dest.mkdirs();
			}

			// list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				// construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				// recursive copy
				if (srcFile.isFile())
					copyfile(srcFile, destFile);
				else
					copyFolder(srcFile, destFile);
			}

		}
	}

	static public boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	/**
	 * copy input to output stream - available in several StreamUtils or Streams
	 * classes
	 */
	public static void copy(InputStream input, OutputStream output)
			throws IOException {
		int bytesRead;
		while ((bytesRead = input.read(BUFFER)) != -1) {
			output.write(BUFFER, 0, bytesRead);
		}
	}

	public static void openUrl(String url) throws IOException,
			URISyntaxException {

		if (java.awt.Desktop.isDesktopSupported()) {
			java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

			if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
				java.net.URI uri = new java.net.URI(url);
				desktop.browse(uri);
			}
		}
	}

	public static boolean addmod(String modfilepath, File minecraftjarpath)
			throws Exception {

		File modfile = new File(modfilepath);
		if (!modfile.exists() || modfile.isDirectory()) {
			// JOptionPane.showMessageDialog(new JFrame(),
			// "Please enter a valid file name", "File not found",
			// JOptionPane.ERROR_MESSAGE);
			return false;
		}
		ZipFile mod;
		try {
			mod = new ZipFile(modfile);
		} catch (ZipException zipexception) {
			return false;
		}

		File jarfile = minecraftjarpath;//
		ZipFile minecraftjar = new ZipFile(jarfile);

		File tempfolder = Minecraft.getAppDir("kirby0016ModInstaller");
		tempfolder.mkdirs();

		ZipOutputStream newjar = new ZipOutputStream(new FileOutputStream(
				new File(tempfolder, "minecraft.jar")));
		
		List<String> dontcopytojar = new ArrayList<String>();

		List<String> alreadycopied = new ArrayList<String>();

		Enumeration<? extends ZipEntry> modentries = mod.entries();
		// check if there is only one folder in root directory
		//
		// Enumeration<? extends ZipEntry> modentries2 = mod.entries();
		// boolean hasmore = false;
		// while (modentries2.hasMoreElements()) {
		// ZipEntry zipEntry = (ZipEntry) modentries2.nextElement();
		// String[] pathparts = zipEntry.getName().split("/");
		// if (pathparts.length >= 1) {
		//
		// }
		// }
		// if (mod) {
		// System.out.println("HI");
		// return false;
		// }

		while (modentries.hasMoreElements()) {
			ZipEntry zipEntry = (ZipEntry) modentries.nextElement();

			String[] foldername = zipEntry.getName().split("/");

			if (ModpartExternalFolder.checkshoulddo(foldername[0], zipEntry,
					"mods")) {
				ModpartExternalFolder.doIO(tempfolder, zipEntry, dontcopytojar,
						mod, foldername[0], File.separator + "mods"
								+ File.separator);
			}

			if (ModpartExternalFolder.checkshoulddo(foldername[0], zipEntry,
					"resources")) {
				ModpartExternalFolder.doIO(tempfolder, zipEntry, dontcopytojar,
						mod, foldername[0], File.separator + "resources"
								+ File.separator);
			}

			if (ModpartDotMinecraftFolder
					.checkshoulddo(foldername[0], zipEntry)) {
				ModpartDotMinecraftFolder.doIO(tempfolder, zipEntry,
						dontcopytojar, mod, foldername[0], File.separator);
			}

			if (ModpartJar.checkshoulddo(foldername[0], zipEntry)) {
				ModpartJar.doIO(tempfolder, zipEntry, dontcopytojar, mod,
						newjar, alreadycopied, foldername[0]);
			}
		}
		for (int i = 0; i < dontcopytojar.size(); i++) {
			File file = new File(dontcopytojar.get(i));
			if (file.isDirectory() && file.exists()) {
				deleteDirectory(file);
			}
		}

		// read minecraft.jar and write to new jar
		// copy contents from existing jar
		Enumeration<? extends ZipEntry> entries = minecraftjar.entries();
		while (entries.hasMoreElements()) {
			ZipEntry e = null;
			try {
				e = entries.nextElement();
			} catch (NullPointerException e1) {
				e1.printStackTrace();
			}

			// only go on if entry won't be replaced by mod-file and is not
			// META-INF
			if (e != null
					&& mod.getEntry(e.getName()) == null
					&& !e.getName().startsWith("META-INF/")
					&& !dontcopytojar
							.contains(new File(tempfolder, e.getName())
									.getPath())
					&& !alreadycopied.contains(e.getName())) { // make sure that
																// it hasn't
																// been copied
																// form jar
																// folder
				newjar.putNextEntry(new ZipEntry(e.getName()));
				if (!e.isDirectory()) {
					copy(minecraftjar.getInputStream(e), newjar);
				}
			}
			newjar.closeEntry();
		}

		// append extra content
		Enumeration<? extends ZipEntry> modfileentries = mod.entries();
		while (modfileentries.hasMoreElements()) {
			ZipEntry e = modfileentries.nextElement();
			if (!dontcopytojar.contains(e.getName())) {
				newjar.putNextEntry(new ZipEntry(e.getName())); // create copy
																// of entry to
																// avoid
																// ZipEntry
																// Exception
				if (!e.isDirectory()) {
					copy(mod.getInputStream(e), newjar);
				}
			}
			newjar.closeEntry();
		}
		newjar.closeEntry();

		// close
		minecraftjar.close();
		newjar.close();
		File tempjarfile = new File(tempfolder, "minecraft.jar");
		copyfile(tempjarfile, jarfile); // finally copy modified jar to
										// destination
		tempjarfile.delete();
		tempfolder.delete();

		// inform user that installation is finished:
		// JOptionPane.showMessageDialog(new JFrame(),
		// "The installation is finished", "Finished",
		// JOptionPane.INFORMATION_MESSAGE);
		return true;
	}
	
	public static boolean addAddon(File modfile)
			throws Exception {
	//	File modfile = new File(modfilepath);
		if (!modfile.exists() || modfile.isDirectory()) {
			return false;
		}
		ZipFile mod;
		try {
			mod = new ZipFile(modfile);
		} catch (ZipException zipexception) {
			return false;
		}
		File tempfolder = new File(Util.getWorkingDirectory(), "tmp");
		tempfolder.mkdirs();

		Enumeration<? extends ZipEntry> modentries = mod.entries();
		
		//dummy
		List<String> dontcopytojar = new ArrayList<String>();
		boolean containsclasses = false;

		while (modentries.hasMoreElements()) {			
			ZipEntry zipEntry = (ZipEntry) modentries.nextElement();
			String foldername = zipEntry.getName().split("/")[0];
			
			if (ModpartExternalFolder.checkshoulddo(foldername, zipEntry,
					"addons")) {
				ModpartExternalFolder.doIO(tempfolder, zipEntry, dontcopytojar,
						mod, foldername, File.separator + "addons"
								+ File.separator);
			}

			if (ModpartResourcesFolder.checkshoulddo(foldername, zipEntry)) {
				ModpartResourcesFolder.doIO(tempfolder, zipEntry, dontcopytojar,
						mod, foldername);
			}

			if (ModpartDotMinecraftFolder
					.checkshoulddo(foldername, zipEntry)) {
				ModpartDotMinecraftFolder.doIO(tempfolder, zipEntry,
						dontcopytojar, mod, foldername, File.separator);
			}
			
			if (ModpartConfigFile
					.checkshoulddo(foldername, zipEntry)) {
				ModpartConfigFile.doIO(zipEntry, mod);
			}
			
			if (zipEntry.getName().endsWith(".class"))
				containsclasses = true;
		}
		
		mod.close();
		
		//TODO support
		
		//copy to mods folder
		if (containsclasses) {
			copyfile(modfile, new File(AddonPanel.addonsfolder, modfile.getName()));
		}
		return true;
	}
}