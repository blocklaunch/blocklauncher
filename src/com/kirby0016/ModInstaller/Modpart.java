package com.kirby0016.ModInstaller;

import java.io.File;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public abstract class Modpart {
	/**
	 * this does the logic of
	 * 
	 * @param tempfolder
	 * @param zipEntry
	 * @param dontcopytojar
	 * @return whether the child function should go on or not
	 */
	public static boolean doIO(File tempfolder, ZipEntry zipEntry,
			List<String> dontcopytojar, ZipFile mod) throws Exception {
		File file = new File(tempfolder, zipEntry.getName());
		dontcopytojar.add(file.getPath());

		if (zipEntry.isDirectory()) {
			file.mkdirs();
			return false;
		}

		File parent = file.getParentFile();
		if (parent != null) {
			parent.mkdirs();
		}

		return true;
	}

	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry) {
		return false;
	}
}
