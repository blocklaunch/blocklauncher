package com.kirby0016.ModInstaller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.blocklaunch.start.Util;

public class ModpartExternalFolder extends Modpart {
	/**
	 * 
	 * @param tempfolder
	 * @param zipEntry
	 * @param dontcopytojar
	 * @param mod
	 * @param foldername
	 * @param copyto
	 *            the folder to copy it to (inside .minecraft) eg:
	 *            File.separator + "mods" + File.separator
	 * @return
	 * @throws Exception
	 */
	public static boolean doIO(File tempfolder, ZipEntry zipEntry,
			List<String> dontcopytojar, ZipFile mod, String foldername,
			String copyto) throws Exception {
		if (!Modpart.doIO(tempfolder, zipEntry, dontcopytojar, mod))
			return false;

		File file = new File(tempfolder, zipEntry.getName().replace("/",
				File.separator));

		InputStream is = mod.getInputStream(zipEntry);
		FileOutputStream fos = new FileOutputStream(file);
		ModIO.copy(is, fos);
		is.close();
		fos.close();

		File folder = new File(tempfolder, foldername + File.separator);

		
		ModIO.copyFolder(folder, new File(Util.getWorkingDirectory(),
				copyto));
		dontcopytojar.add(folder.getPath());
		file.delete();

		return true;
	}

	/**
	 * 
	 * @param foldername
	 * @param zipEntry
	 * @param copytowithoutsep
	 *            also the folder to copy to, but without "File.seperator"s eg:
	 *            "mods"
	 * @return whether the doIO should be done or not
	 */
	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry,
			String copytowithoutsep) {
		return (foldername
				.toLowerCase().contains("put") && foldername.toLowerCase()
				.contains(copytowithoutsep));
	}

}
