package com.kirby0016.ModInstaller;

import java.io.File;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ModpartResourcesFolder extends ModpartExternalFolder {
	
	/**
	 * 
	 * @param tempfolder
	 * @param zipEntry
	 * @param dontcopytojar
	 * @param mod
	 * @param foldername
	 * @param copyto
	 *            the folder to copy it to (inside .minecraft) eg:
	 *            File.separator + "mods" + File.separator
	 * @return
	 * @throws Exception
	 */
	public static boolean doIO(File tempfolder, ZipEntry zipEntry,
			List<String> dontcopytojar, ZipFile mod, String foldername) throws Exception {
		
		return ModpartExternalFolder.doIO(tempfolder, zipEntry,
			dontcopytojar, mod, foldername,
			File.separator + "resources"
					+ File.separator);
		
	}
	
	/**
	 * 
	 * @param foldername
	 * @param zipEntry
	 * @param copytowithoutsep
	 *            also the folder to copy to, but without "File.seperator"s eg:
	 *            "mods"
	 * @return whether the doIO should be done or not
	 */
	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry) {
		return (zipEntry.getName().toLowerCase()
				.startsWith("resources/") || ModpartExternalFolder
				.checkshoulddo(foldername, zipEntry, "resources"));
	}
}
