package com.kirby0016.ModInstaller;

import java.io.*;

import javax.swing.JFrame;

public class Minecraft {
	public static File getAppDir(String s) {
		String s1 = System.getProperty("user.home", ".");
		File file;
		switch (getOs()) {
		case 1:
		case 2:
			file = new File(s1, (new StringBuilder()).append('.').append(s)
					.append('/').toString());
			break;

		case 3: // WINDOWS
			String s2 = System.getenv("APPDATA");
			if (s2 != null) {
				file = new File(s2, (new StringBuilder()).append(".").append(s)
						.append('/').toString());
			} else {
				file = new File(s1, (new StringBuilder()).append('.').append(s)
						.append('/').toString());
			}
			break;

		case 4: // MAC
			file = new File(s1, (new StringBuilder())
					.append("Library/Application Support/").append(s)
					.toString());
			break;

		default:
			file = new File(s1, (new StringBuilder()).append(s).append('/')
					.toString());
			break;
		}
		if (!file.exists() && !file.mkdirs()) {
			throw new RuntimeException((new StringBuilder())
					.append("The working directory could not be created: ")
					.append(file).toString());
		} else {
			return file;
		}
	}

	private static int getOs() {
		String osName = System.getProperty("os.name").toLowerCase();

		if (osName.contains("linux"))
			return 1;
		if (osName.contains("unix"))
			return 1;
		if (osName.contains("solaris"))
			return 2;
		if (osName.contains("sunos"))
			return 2;
		if (osName.contains("win"))
			return 3;
		if (osName.contains("mac"))
			return 4;
		return 5;
	}

	public static void AddLangFile(File file, String language) {
		BufferedReader bufferedreader = null;
		try {
			bufferedreader = new BufferedReader(new FileReader(file));
			for (String s1 = bufferedreader.readLine(); s1 != null; s1 = bufferedreader
					.readLine()) {
				s1 = s1.trim();
				if (s1.startsWith("#")) {
					continue;
				}
				String as[] = s1.split("=");
				if (as != null && as.length == 2) {
					if (!file.getAbsolutePath().endsWith("languages.txt"))
						AddKey(as[0], as[1], language);
					else
						AddKey(as[0], as[1]);
				}
			}
		} catch (IOException e) {
		} finally {
			try {
				bufferedreader.close();
			} catch (Exception e) {
			}
		}
	}

	public static void AddFiles(String fileList[], JFrame shlMain) {
		boolean success = true;
		for (int i = 0; i < fileList.length; i++)
			if (fileList != null
					&& (fileList[i].endsWith(".lang") || fileList[i]
							.endsWith("languages.txt"))) {
				try {
					File file = new File(fileList[i]);
					// AddKey(as[0], as[1], language);
					String name = file.getName().replace(".lang", "");
					// File minecraftdir = Minecraft.getAppDir("minecraft");
					file.createNewFile();
					Minecraft.AddLangFile(file, name);
				} catch (IOException e) {
					success = false;
				}
			} else {
				// MessageBox msg = new MessageBox(shlMain, SWT.ICON_ERROR);
				// msg.setMessage("This is not a valid Languagefile!");
				// msg.open();
				// success = false;
			}
		if (success) {
			// MessageBox msg = new MessageBox(shlMain, SWT.OK);
			// msg.setMessage("Successfully added the translations");
			// msg.setText("Success!");
			// msg.open();
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}

	public static void AddKey(String key, String value, String language) {
		File moddir = new File(getAppDir("blocklaunch"), "/mods/LanguageApi/");
		File languagefile = new File(moddir, language + ".lang");
		BufferedWriter writer = null;
		try {
			moddir.mkdirs();
			languagefile.createNewFile();

			writer = new BufferedWriter(new FileWriter(languagefile, true));

			if (translateKey(key, language) == null) {
				writer.newLine();
				writer.write(key + "=" + value);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	public static void AddKey(String key, String value) {
		File moddir = new File(getAppDir("blocklaunch"), "/mods/LanguageApi/");
		File languagefile = new File(moddir, "languages.txt");
		BufferedWriter writer = null;
		try {
			moddir.mkdirs();
			languagefile.createNewFile();

			writer = new BufferedWriter(new FileWriter(languagefile, true));

			if (checklanguageexists(key) == null) {
				writer.newLine();
				writer.write(key + "=" + value);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	public static String translateKey(String s, String lang) {
		File languagefile = new File(getAppDir("blocklaunch"),
				"/mods/LanguageApi/" + lang + ".lang");
		BufferedReader bufferedreader = null;
		try {
			languagefile.createNewFile();

			bufferedreader = new BufferedReader(new FileReader(languagefile));
			for (String s1 = bufferedreader.readLine(); s1 != null; s1 = bufferedreader
					.readLine()) {
				s1 = s1.trim();
				if (s1.startsWith("#")) {
					continue;
				}
				String as[] = s1.split("=");
				if (as != null && as.length == 2) {
					if (as[0].equals(s)) {
						return as[1];
					}
				}
			}
		} catch (IOException e) {
		} finally {
			try {
				bufferedreader.close();
			} catch (Exception e) {
			}
		}
		return null;
	}

	public static String checklanguageexists(String s) {
		File languagefile = new File(getAppDir("blocklaunch"),
				"/mods/LanguageApi/" + "languages.txt");
		BufferedReader bufferedreader = null;
		try {
			languagefile.createNewFile();

			bufferedreader = new BufferedReader(new FileReader(languagefile));
			for (String s1 = bufferedreader.readLine(); s1 != null; s1 = bufferedreader
					.readLine()) {
				s1 = s1.trim();
				if (s1.startsWith("#")) {
					continue;
				}
				String as[] = s1.split("=");
				if (as != null && as.length == 2) {
					if (as[0].equals(s)) {
						return as[1];
					}
				}
			}
		} catch (IOException e) {
		} finally {
			try {
				bufferedreader.close();
			} catch (Exception e) {
			}
		}
		return null;
	}
}
