package com.kirby0016.ModInstaller;

import java.util.zip.ZipEntry;

public class ModpartDotMinecraftFolder extends ModpartExternalFolder {

	/**
	 * 
	 * @param foldername
	 * @param zipEntry
	 * @param copytowithoutsep
	 *            also the folder to copy to, but without "File.seperator"s eg:
	 *            "mods"
	 * @return whether the doIO should be done or not
	 */
	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry) {
		return (foldername.toLowerCase().contains("put") && foldername
				.toLowerCase().contains(".minecraft"))
				|| (foldername.toLowerCase().contains("put")
						&& foldername.toLowerCase().contains("minecraft") && foldername
						.toLowerCase().contains("folder"));
	}
}
