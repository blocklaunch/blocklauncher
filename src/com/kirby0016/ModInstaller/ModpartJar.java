package com.kirby0016.ModInstaller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ModpartJar extends Modpart {

	/**
	 * this does the logic of
	 * 
	 * @param tempfolder
	 * @param zipEntry
	 * @param dontcopytojar
	 * @return
	 */
	public static boolean doIO(File tempfolder, ZipEntry zipEntry,
			List<String> dontcopytojar, ZipFile mod, ZipOutputStream newjar,
			List<String> alreadycopied, String foldername) throws Exception {
		if (!Modpart.doIO(tempfolder, zipEntry, dontcopytojar, mod))
			return false;

		File file = new File(tempfolder, zipEntry.getName().replace("/",
				File.separator));

		InputStream is = mod.getInputStream(zipEntry);
		FileOutputStream fos = new FileOutputStream(file);
		ModIO.copy(is, fos);
		is.close();
		fos.close();

		// append file
		String fileinjarfolder = "";
		if (zipEntry.getName().toLowerCase().startsWith("jar/")) {
			fileinjarfolder = file.getPath().substring(4);
		} else if (zipEntry.getName().toLowerCase()
				.startsWith("minecraft.jar/")) {
			fileinjarfolder = file.getPath().substring(14);
		} else if (zipEntry.getName().toLowerCase().contains("put")
				&& zipEntry.getName().toLowerCase().contains("jar")) {
			fileinjarfolder = file.getPath()
					.replace(tempfolder.getPath() + File.separator, "")
					.replace(foldername + File.separator, "");
		} else if (zipEntry.getName().toLowerCase().contains("classes")) {
			fileinjarfolder = file.getPath()
					.replace(tempfolder.getPath() + File.separator, "")
					.replace(foldername + File.separator, "");
		}
		fileinjarfolder = fileinjarfolder.replace(File.separator, "/");

		ZipEntry e = new ZipEntry(fileinjarfolder); // file.getName()
		FileInputStream in = new FileInputStream(file);
		// System.out.println(e.getName());
		newjar.putNextEntry(e); // create zip entry from file
		alreadycopied.add(e.getName()); // add entry name to copied list
		// if (!e.isDirectory()) {
		ModIO.copy(in, newjar);
		// }

		newjar.closeEntry();
		in.close();

		file.delete();
		return true;
	}

	public static boolean checkshoulddo(String foldername, ZipEntry zipEntry) {
		return (zipEntry.getName().toLowerCase().startsWith("jar/")
				|| zipEntry.getName().toLowerCase()
						.startsWith("minecraft.jar/")
				|| (foldername.toLowerCase().contains("put") && foldername
						.toLowerCase().contains("jar")) || (foldername
					.toLowerCase().contains("classes")));
	}

}
