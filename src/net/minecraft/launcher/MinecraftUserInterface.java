package net.minecraft.launcher;

import net.minecraft.launcher.game.MinecraftGameRunner;
import net.minecraft.launcher.ui.LauncherPanel;

import com.mojang.launcher.UserInterface;
import com.mojang.launcher.events.GameOutputLogProcessor;

public abstract interface MinecraftUserInterface
  extends UserInterface
{
  public abstract void showOutdatedNotice();
  
  public abstract String getTitle();
  
  public abstract LauncherPanel getLauncherPanel();
  
  public abstract GameOutputLogProcessor showGameOutputTab(MinecraftGameRunner paramMinecraftGameRunner);

}