 package net.minecraft.launcher.updater;
 
 import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import net.minecraft.launcher.game.MinecraftReleaseType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.blocklaunch.Link;
import org.blocklaunch.OnlinetxttoArray;
import org.blocklaunch.start.Util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.mojang.launcher.events.RefreshedVersionsListener;
import com.mojang.launcher.updater.ExceptionalThreadPoolExecutor;
import com.mojang.launcher.updater.VersionFilter;
import com.mojang.launcher.updater.VersionManager;
import com.mojang.launcher.updater.VersionSyncInfo;
import com.mojang.launcher.updater.download.DownloadJob;
import com.mojang.launcher.updater.download.Downloadable;
import com.mojang.launcher.updater.download.EtagDownloadable;
import com.mojang.launcher.updater.download.assets.AssetDownloadable;
import com.mojang.launcher.updater.download.assets.AssetIndex;
import com.mojang.launcher.versions.CompleteVersion;
import com.mojang.launcher.versions.Version;
import com.mojang.launcher.OperatingSystem;

 public class MinecraftVersionManager implements VersionManager
 {
   private static final Logger LOGGER = LogManager.getLogger();
   private final VersionList localVersionList;
   private final VersionList remoteVersionList;
   private final ThreadPoolExecutor executorService = new ExceptionalThreadPoolExecutor(4, 8, 30L, TimeUnit.SECONDS);
   private final List<RefreshedVersionsListener> refreshedVersionsListeners = Collections.<RefreshedVersionsListener>synchronizedList(new ArrayList<RefreshedVersionsListener>());
   private final Object refreshLock = new Object();
   private boolean isRefreshing;
   private final Gson gson = new Gson();
 
   public MinecraftVersionManager(VersionList localVersionList, VersionList remoteVersionList) {
     this.localVersionList = localVersionList;
     this.remoteVersionList = remoteVersionList;
   }
 
   public void refreshVersions() throws IOException {
     synchronized (this.refreshLock) {
       this.isRefreshing = true;
     }
     try
     {
       LOGGER.info("Refreshing local version list...");
       this.localVersionList.refreshVersions();
       LOGGER.info("Refreshing remote version list...");
       this.remoteVersionList.refreshVersions();
     } catch (IOException ex) {
       synchronized (this.refreshLock) {
         this.isRefreshing = false;
       }
       throw ex;
     }
 
     LOGGER.info("Refresh complete.");
 
     synchronized (this.refreshLock) {
       this.isRefreshing = false;
     }
 
     for (RefreshedVersionsListener listener : Lists.newArrayList(this.refreshedVersionsListeners)) {
         listener.onVersionsRefreshed(this);
       }
   }
 
   public List<VersionSyncInfo> getVersions()
   {
     return getVersions(null);
   }
 
   public List<VersionSyncInfo> getVersions(VersionFilter filter) {
     synchronized (this.refreshLock) {
       if (this.isRefreshing) return new ArrayList();
     }
 
     List<VersionSyncInfo> result = new ArrayList();
     Object lookup = new HashMap();
     Map<MinecraftReleaseType, Integer> counts = Maps.newEnumMap(MinecraftReleaseType.class);
     for (MinecraftReleaseType type : MinecraftReleaseType.values()) {
       counts.put(type, Integer.valueOf(0));
     }
     for (Version version : this.localVersionList.getVersions()) {
       if ((version.getType() != null) && (version.getUpdatedTime() != null))
       {
         MinecraftReleaseType type = (MinecraftReleaseType)version.getType();
         if ((filter == null) || ((filter.getTypes().contains(type)) && (((Integer)counts.get(type)).intValue() < filter.getMaxCount())))
         {
           VersionSyncInfo syncInfo = getVersionSyncInfo(version, this.remoteVersionList.getVersion(version.getId()));
           ((Map)lookup).put(version.getId(), syncInfo);
           result.add(syncInfo);
         }
       }
     }
     for (Version version : this.remoteVersionList.getVersions()) {
         if ((version.getType() != null) && (version.getUpdatedTime() != null))
         {
           MinecraftReleaseType type = (MinecraftReleaseType)version.getType();
           if ((!((Map)lookup).containsKey(version.getId())) && (
             (filter == null) || ((filter.getTypes().contains(type)) && (((Integer)counts.get(type)).intValue() < filter.getMaxCount()))))
           {
             VersionSyncInfo syncInfo = getVersionSyncInfo(this.localVersionList.getVersion(version.getId()), version);
             ((Map)lookup).put(version.getId(), syncInfo);
             result.add(syncInfo);
             if (filter != null) {
               counts.put(type, Integer.valueOf(((Integer)counts.get(type)).intValue() + 1));
             }
           }
         }
       }
     if (result.isEmpty()) {
       for (Version version : this.localVersionList.getVersions()) {
         if ((version.getType() != null) && (version.getUpdatedTime() != null)) {
           VersionSyncInfo syncInfo = getVersionSyncInfo(version, this.remoteVersionList.getVersion(version.getId()));
           ((Map)lookup).put(version.getId(), syncInfo);
           result.add(syncInfo);
         }
       }
     }
     Collections.sort(result, new Comparator() {
         public int compare(Object a, Object b) {
             Version aVer = ((VersionSyncInfo) a).getLatestVersion();
             Version bVer = ((VersionSyncInfo) b).getLatestVersion();

             if ((aVer.getReleaseTime() != null) && (bVer.getReleaseTime() != null)) {
                 return bVer.getReleaseTime().compareTo(aVer.getReleaseTime());
             }
             return bVer.getUpdatedTime().compareTo(aVer.getUpdatedTime());
         }
     });
     return result;
   }
 
   public VersionSyncInfo getVersionSyncInfo(Version version) {
     return getVersionSyncInfo(version.getId());
   }
 
   public VersionSyncInfo getVersionSyncInfo(String name) {
     return getVersionSyncInfo(this.localVersionList.getVersion(name), this.remoteVersionList.getVersion(name));
   }
 
   public VersionSyncInfo getVersionSyncInfo(Version localVersion, Version remoteVersion) {
     boolean installed = localVersion != null;
     boolean upToDate = installed;
 
     if ((installed) && (remoteVersion != null)) {
       upToDate = !remoteVersion.getUpdatedTime().after(localVersion.getUpdatedTime());
     }
     if ((localVersion instanceof CompleteMinecraftVersion)) {
       upToDate &= this.localVersionList.hasAllFiles((CompleteMinecraftVersion)localVersion, OperatingSystem.getCurrentPlatform());
     }
 
     return new VersionSyncInfo(localVersion, remoteVersion, installed, upToDate);
   }
 
   public List<VersionSyncInfo> getInstalledVersions() {
     List result = new ArrayList();
 
     for (Version version : this.localVersionList.getVersions()) {
       if ((version.getType() != null) && (version.getUpdatedTime() != null))
       {
         VersionSyncInfo syncInfo = getVersionSyncInfo(version, this.remoteVersionList.getVersion(version.getId()));
         result.add(syncInfo);
       }
     }
     return result;
   }
 
   public VersionList getRemoteVersionList() {
     return this.remoteVersionList;
   }
 
   public VersionList getLocalVersionList() {
     return this.localVersionList;
   }
 
   public CompleteMinecraftVersion getLatestCompleteVersion(VersionSyncInfo syncInfo) throws IOException {
     if (syncInfo.getLatestSource() == VersionSyncInfo.VersionSource.REMOTE) {
       CompleteMinecraftVersion result = null;
       IOException exception = null;
       try
       {
         result = this.remoteVersionList.getCompleteVersion(syncInfo.getLatestVersion());
       } catch (IOException e) {
         exception = e;
         try {
           result = this.localVersionList.getCompleteVersion(syncInfo.getLatestVersion());
         } catch (IOException localIOException1) {
         }
       }
       if (result != null) {
         return result;
       }
       throw exception;
     }
 
     return this.localVersionList.getCompleteVersion(syncInfo.getLatestVersion());
   }
   
 public DownloadJob downloadVersion(VersionSyncInfo syncInfo, DownloadJob job) throws IOException
   {
     if (!(this.localVersionList instanceof LocalVersionList)) throw new IllegalArgumentException("Cannot download if local repo isn't a LocalVersionList");
     if (!(this.remoteVersionList instanceof RemoteVersionList)) throw new IllegalArgumentException("Cannot download if local repo isn't a RemoteVersionList");
     CompleteMinecraftVersion version = getLatestCompleteVersion(syncInfo);
     File baseDirectory = ((LocalVersionList)this.localVersionList).getBaseDirectory();
     Proxy proxy = ((RemoteVersionList)this.remoteVersionList).getProxy();
 
     job.addDownloadables(version.getRequiredDownloadables(OperatingSystem.getCurrentPlatform(), proxy, baseDirectory, false));
 
//     String jarFile = "versions/" + version.getId() + "/" + version.getId() + ".jar";
//     job.addDownloadables(new Downloadable[] { new EtagDownloadable(proxy, new URL("https://s3.amazonaws.com/Minecraft.Download/" + jarFile), new File(baseDirectory, jarFile), false) });
 
   //new+changed        
     String[] VersionParts = version.getId().split(Pattern.quote("_"));
     String version2;
     boolean BLmode = false;
     if (VersionParts[0].equalsIgnoreCase("BL")){
     	version2 = VersionParts[2];
     	BLmode = true;
     }else{
     	version2 = version.getId();
     	BLmode = false;}
     
     if (BLmode){  	 
    	//BLmode
     	//TODO add Mods
     	String jarFileDL = "versions/" + version2 + "/" + version2 + ".jar";
     	String jarFile = "versions/" + version.getId() + "/" + version.getId() + ".jar";
     	//Dl-MC-jar
     	job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL("https://s3.amazonaws.com/Minecraft.Download/" + jarFileDL), new File(baseDirectory, jarFile), false)});
     	//mods-installer
     	LOGGER.info("Enabled BlockLaunch - downloading Mods");
     	
     	String modsfolder = "versions"+File.separator+version.getId()+File.separator+"mods"+File.separator;
     	File modfolder = new File(baseDirectory.toString(), modsfolder);
     	//Base-Mods
     	String basefolder = "versions"+File.separator+version.getId()+File.separator+"basemods"+File.separator;
     	File basfolder = new File(baseDirectory.toString(), basefolder);
     	//Forge
     	String Forge = "versions"+File.separator+version.getId()+File.separator+"forge"+File.separator;
     	File forgefolder = new File(baseDirectory.toString(), Forge);        	
     	
     	//Mods
     	if (!(modfolder.exists())){
     	modfolder.mkdirs();}
     	//base-Mods
     	if (!(basfolder.exists())){
         	basfolder.mkdirs();}        	
//     	//forge
     	if (!(forgefolder.exists())){
         	forgefolder.mkdirs();}
     	        	
     	String modsurl = (Link.DL_URL+"versions/"+version.getId()+"/mods.txt");
     	
     	//get Forge
     	Vector<String> forge = OnlinetxttoArray.getForge(modsurl);
     	//Get Basemods
     	Vector<String> base = OnlinetxttoArray.getBase(modsurl);        	
     	//get Mods
     	Vector<String> mods = OnlinetxttoArray.getMod(modsurl);
     	////
     	LOGGER.info("mods" + mods.toString());
     	if (mods.isEmpty()){
     		LOGGER.info("ERROR while downloading modslist");
     	}
     	String name;
     	String ModURL;
     	File ModFile;
     	
     	//nicht f�r 1.5.2/1.5.1
         if (!((version.getId().endsWith("1.5.2"))||(version.getId().endsWith("1.5.1")))){
     	
         LOGGER.info("Downloading Forge");
     	
     		File Forge2 = new File(forge.get(0));
     	
     		System.out.println(Forge2.toString());
     	
     		String[] forgesplit = (Forge2.getName().substring(0, Forge2.getName().length()-4)).split("-");
// 		for (int i = 0; i <forgesplit.length; i++)    	System.out.println(forgesplit[i]);
     	
     		String libaryonly;
     		String forgelibaryname;
     	
 		if (version.getId().endsWith("1.6.4")){
 			String forgeversion = forgesplit[3];
 	 		
 	 		System.out.println("Forge:" + forgeversion);
 			libaryonly = "libraries"+File.separator+"net"+File.separator+"minecraftforge"+File.separator+"minecraftforge"+File.separator+forgeversion;
 			forgelibaryname = "minecraftforge-" + forgeversion+".jar";
 		}else{
 			String forgeversion = forgesplit[1]+"-"+forgesplit[2];
 	 		
 	 		System.out.println("Forge:" + forgeversion);
 			libaryonly = "libraries"+File.separator+"net"+File.separator+"minecraftforge"+File.separator+"forge"+File.separator+forgeversion;
 			forgelibaryname = "forge-" + forgeversion+".jar";
 			//libs
 			
 		}
 		
 		File libary = new File(baseDirectory, libaryonly);
 		
 		
 		File forgelibary = new File(libary, forgelibaryname);
 		
 		System.out.println(forgelibary.toString());
 		
 		if (!forgelibary.exists()){
     		//DL
     		job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL(forge.get(0)), forgelibary, false)});//}else{
 		}
         }else{ LOGGER.info("Skip Downloading Forgelibrary");}
 			
         
         if ((version.getId().endsWith("1.5.2"))||(version.getId().endsWith("1.5.1"))){
	        	 LOGGER.info("Downlaoding Forge Libraries");
	    		  //alte versionen (1.0+1.0.1
	    		//oldlibs
	    			 Vector<String> libs = OnlinetxttoArray.getOldForgelib(modsurl);
	    			 String LibURL;
	    	     	for (int i = 0; (i < (libs.size())); i++){
	    	     		LibURL = libs.get(i); 	         		
	    	     		name = new File(LibURL).getName(); 	         		
	    	     		System.out.println("1Lib "+i + LibURL); 	         		
	    	     		File libfolder = new File(Util.getWorkingDirectory("minecraft"), "lib");
	    	     		if(!libfolder.exists()){libfolder.mkdirs();}
	    	     		File LibFile = new File(libfolder, name); 
	         		
	    	     		System.out.println("2Lib "+i + LibFile);
	    	     		//DL
	    	     		if(!LibFile.exists()){ 	    	     			
	    	     			job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL(LibURL), LibFile, false)});
	    	     		}
	    	     	}
         }
     	LOGGER.info("Downloading Basemods");
     	
     	for (int i = 0; (i < (base.size())); i++){
     		
     		ModURL = base.get(i);
     		
     		name = new File(ModURL).getName();
     		
     		System.out.println("1F "+i + ModURL);
     		
     		ModFile = new File(basfolder, name); 
     		
     		System.out.println("2F "+i + ModFile);
     		
     		if(!ModFile.exists()){
     		//DL
     		job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL(ModURL), ModFile, false)});//}else{
     		}
     	} 
     	
     	
     	LOGGER.info("Downloading Mods");
     	
     	for (int i = 0; (i < (mods.size())); i++){
     		
     		ModURL = mods.get(i);
     		
     		name = new File(ModURL).getName();
     		
     		System.out.println("1"+i + ModURL);
     		
     		ModFile = new File(modfolder, name); 
     		
     		System.out.println("2"+i + ModFile);
     		if (!ModFile.exists()){
     		//DL
     		job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL(ModURL), ModFile, false)});//}else{
     		}
     	}
     	
     }else{
     	String jarFile = "versions/" + version2 + "/" + version2 + ".jar";
     	job.addDownloadables(new Downloadable[]{new EtagDownloadable(proxy, new URL("https://s3.amazonaws.com/Minecraft.Download/" + jarFile), new File(baseDirectory, jarFile), false)});
     }
     return job;
 }

 
 ///ENDE
 
   public DownloadJob downloadResources(DownloadJob job, CompleteMinecraftVersion version) throws IOException {
     File baseDirectory = ((LocalVersionList)this.localVersionList).getBaseDirectory();
 
     job.addDownloadables(getResourceFiles(((RemoteVersionList)this.remoteVersionList).getProxy(), baseDirectory, version));
 
     return job;
   }
 
   private Set<Downloadable> getResourceFiles(Proxy proxy, File baseDirectory, CompleteMinecraftVersion version)
   {
     Set<Downloadable> result = new HashSet();
     InputStream inputStream = null;
     File assets = new File(baseDirectory, "assets");
     File objectsFolder = new File(assets, "objects");
     File indexesFolder = new File(assets, "indexes");
     String indexName = version.getAssets();
     long start = System.nanoTime();
     if (indexName == null) {
       indexName = "legacy";
     }
     File indexFile = new File(indexesFolder, indexName + ".json");
     try
     {
       URL indexUrl = this.remoteVersionList.getUrl("indexes/" + indexName + ".json");
       inputStream = indexUrl.openConnection(proxy).getInputStream();
       String json = IOUtils.toString(inputStream);
       FileUtils.writeStringToFile(indexFile, json);
       AssetIndex index = (AssetIndex)this.gson.fromJson(json, AssetIndex.class);
       for (Map.Entry<AssetIndex.AssetObject, String> entry : index.getUniqueObjects().entrySet())
       {
         AssetIndex.AssetObject object = (AssetIndex.AssetObject)entry.getKey();
         String filename = object.getHash().substring(0, 2) + "/" + object.getHash();
         File file = new File(objectsFolder, filename);
         if ((!file.isFile()) || (FileUtils.sizeOf(file) != object.getSize()))
         {
           Downloadable downloadable = new AssetDownloadable(proxy, (String)entry.getValue(), object, "http://resources.download.minecraft.net/", objectsFolder);
           downloadable.setExpectedSize(object.getSize());
           result.add(downloadable);
         }
       }
       long end = System.nanoTime();
       long delta = end - start;
       LOGGER.debug("Delta time to compare resources: " + delta / 1000000L + " ms ");
     }
     catch (Exception ex)
     {
       LOGGER.error("Couldn't download resources", ex);
     }
     finally
     {
       IOUtils.closeQuietly(inputStream);
     }
     return result;
   }
 
   public ThreadPoolExecutor getExecutorService() {
     return this.executorService;
   }
 
   public void addRefreshedVersionsListener(RefreshedVersionsListener listener) {
     this.refreshedVersionsListeners.add(listener);
   }
 
   public void removeRefreshedVersionsListener(RefreshedVersionsListener listener) {
     this.refreshedVersionsListeners.remove(listener);
   }

   public VersionSyncInfo syncVersion(VersionSyncInfo syncInfo)
		    throws IOException
		  {
		    CompleteVersion remoteVersion = getRemoteVersionList().getCompleteVersion(syncInfo.getRemoteVersion());
		    getLocalVersionList().removeVersion(syncInfo.getLocalVersion());
		    getLocalVersionList().addVersion(remoteVersion);
		    ((LocalVersionList)getLocalVersionList()).saveVersion(remoteVersion);
		    return getVersionSyncInfo(remoteVersion);
		  }
		  
		  public void installVersion(CompleteVersion version)
		    throws IOException
		  {
		    VersionList localVersionList = getLocalVersionList();
		    if (localVersionList.getVersion(version.getId()) != null) {
		      localVersionList.removeVersion(version.getId());
		    }
		    localVersionList.addVersion(version);
		    if ((localVersionList instanceof LocalVersionList)) {
		      ((LocalVersionList)localVersionList).saveVersion(version);
		    }
		    LOGGER.info("Installed " + version);
		  }
		  
		  public void uninstallVersion(CompleteVersion version)
		    throws IOException
		  {
		    VersionList localVersionList = getLocalVersionList();
		    if ((localVersionList instanceof LocalVersionList))
		    {
		      localVersionList.uninstallVersion(version);
		      LOGGER.info("Uninstalled " + version);
		    }
		  }

		@Override
		public DownloadJob downloadResources(DownloadJob job,
				CompleteVersion version) throws IOException {
			File baseDirectory = ((LocalVersionList)this.localVersionList).getBaseDirectory();
		    
		    job.addDownloadables(getResourceFiles(((RemoteVersionList)this.remoteVersionList).getProxy(), baseDirectory, (CompleteMinecraftVersion)version));
		    
		    return job;
		}
 }