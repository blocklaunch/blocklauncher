package net.minecraft.launcher.ui.top;

import net.minecraft.launcher.Launcher;

import javax.swing.*;

import org.blocklaunch.serverinstaller.Serverinstalldialog;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class SetupServerButtonPanel extends JPanel{
    private final Launcher launcher;
    private final JButton selectButton = new JButton("Create Server");
    final Serverinstalldialog dialog;

    public SetupServerButtonPanel(Launcher launcher) {
    	//super(launcher);
    	this.launcher = launcher;
    	//durchsichtig wegen textur
    	this.setOpaque(true); 
        createInterface();
        
        dialog = new Serverinstalldialog(launcher);

        this.selectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {                
            	try { 
            		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            		dialog.refreshserverversion();            		
        			
        			//dialog.setVisible(true);
        		} catch (Exception f) {
        			f.printStackTrace();
        		}
            };
            
        });
        
    }
    
    public Serverinstalldialog getdialog(){
		return dialog;    	
    }

    protected void createInterface() {
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = 1;
        constraints.weightx = 1.0D;
        constraints.weighty = 1.0D;

        constraints.gridy = 0;
        constraints.gridx = 0;
        add(this.selectButton, constraints);

      //  this.selectButton.setFont(this.selectButton.getFont().deriveFont(1, this.selectButton.getFont().getSize()));
    }
    
    public boolean shouldReceiveEventsInUIThread() {
        return true;
    }

    public Launcher getLauncher() {
        return this.launcher;
    }

}