package net.minecraft.launcher.ui.tabs;

import javax.swing.JScrollPane;

import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.ui.bottombar.StatusPanelForm;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JButton;

public class OptionsTab extends JScrollPane {

	//private JComboBox comboBox;
	/**
	 * Create the panel.
	 */
	private final Launcher launcher;
	private JTextField textField;
	
	public OptionsTab(Launcher launcher) {
		
		this.launcher = launcher;
		
		JLabel lblOptions = new JLabel("Options");
		setColumnHeaderView(lblOptions);
		
		JPanel panel = new JPanel();
		setViewportView(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{22, 91, 209, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		
		
		JLabel lblDir = new JLabel("Minecraft Location: ");
		GridBagConstraints gbc_lblDir = new GridBagConstraints();
		gbc_lblDir.insets = new Insets(0, 0, 5, 5);
		gbc_lblDir.anchor = GridBagConstraints.EAST;
		gbc_lblDir.gridx = 1;
		gbc_lblDir.gridy = 2;
		panel.add(lblDir, gbc_lblDir);
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		textField.setEditable(false);
		textField.setText(launcher.getLauncher().getWorkingDirectory().toPath().toString());
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 2;
		textField.setText("working directory");
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton btnDir = new JButton("Change");
		btnDir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              //  buttonChangeWDActionPerformed(e);
            }
			
        });
		GridBagConstraints gbc_btnDir = new GridBagConstraints();
		gbc_btnDir.insets = new Insets(0, 0, 5, 0);
		gbc_btnDir.gridx = 3;
		gbc_btnDir.gridy = 2;
		panel.add(btnDir, gbc_btnDir);
		
		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.gridwidth = 2;
		gbc_panel_2.insets = new Insets(0, 0, 0, 2);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 4;
		panel.add(panel_2, gbc_panel_2);
		
		StatusPanelForm panel_1 = new StatusPanelForm(this.launcher);
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 2;
		gbc_panel_1.gridy = 3;
		panel.add(panel_1, gbc_panel_1);}

	
//	private void buttonChangeWDActionPerformed(ActionEvent e) {
//		// TODO Automatisch generierter Methodenstub
//		InstallDirSettings.changeDir(launcher.getLauncher().getUserInterface().getClass().getFrame(), Launcher.getInstance().getWorkingDirectory());
//        Launcher.getInstance().getFrame().getWindowListeners()[0].windowClosing(null);
//        try {
//            Main.main(new String[0]);
//        } catch (IOException ex) {
//            Logger.getLogger(OptionsTab.class.getName()).log(Level.SEVERE, null, ex);
//        }
//	}
	
	public Launcher getLauncher() {
        return this.launcher;
    }

}
