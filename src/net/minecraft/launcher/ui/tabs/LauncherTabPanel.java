 package net.minecraft.launcher.ui.tabs;
 
 import java.awt.*;
 import javax.swing.*;
 import net.minecraft.launcher.Launcher;
 import org.blocklaunch.addon.*;
 
 public class LauncherTabPanel extends JTabbedPane
 {
   private final Launcher launcher;
   private final WebsiteTab blblog;
   private final ConsoleTab console;
   private CrashReportTab crashReportTab;
   
   private final WebsiteTab mcblog;
   private final AddonPanel addon;
 
   public LauncherTabPanel(Launcher launcher)
   {
     super(1);
 
     this.launcher = launcher;
     //BLblog
     this.blblog = new WebsiteTab(launcher);
     this.console = new ConsoleTab(launcher);
     
    //mcblog
     this.mcblog = new WebsiteTab(launcher);
     //addons
     this.addon = new AddonPanel(launcher);
     //tabs rechts
     this.setTabPlacement(RIGHT);
 
     createInterface();
   }
 
   protected void createInterface() {
//     addTab("Update Notes", this.blog);
//     addTab("Development Console", this.console);
//     addTab("Profile Editor", new ProfileListTab(this.launcher));
	 //BlockLaunch Update Info
       addTab("", new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/favicon.png")), this.blblog, "BlockLaunch Update Notes");
       //MC Update Info
       addTab("",new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/favicon_mc.png")), this.mcblog, "Minecraft Update Notes");
       //Console
       addTab("", new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/Console_icon.png")), this.console, "Development Console");
       //addontab
       addTab("",new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/AddonCarton.png")), this.addon, "Addons");
       //profile Editor       
       addTab("", new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/profile_edit.png")), new ProfileListTab(this.launcher),"Profile Editor");
       
   }
 
   public Launcher getLauncher() {
       return this.launcher;
   }

   public WebsiteTab getBLBlog() {
       return this.blblog;
   }

   public AddonPanel getAddonPanel() {
       return this.addon;
   }
   
   public WebsiteTab getMCBlog() {
       return this.mcblog;
   }
   
   public void showConsole() {
     setSelectedComponent(this.console);
   }
 
   public void setCrashReport(CrashReportTab newTab) {
     if (this.crashReportTab != null) removeTab(this.crashReportTab);
     this.crashReportTab = newTab;
//     addTab("Crash Report", this.crashReportTab); //TODO
     addTab("", new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/crash.png")), new ProfileListTab(this.launcher),"Crash Report");     
     setSelectedComponent(newTab);
   }
 
   protected void removeTab(Component tab) {
     for (int i = 0; i < getTabCount(); i++)
       if (getTabComponentAt(i) == tab) {
         removeTabAt(i);
         break;
       }
   }
 }