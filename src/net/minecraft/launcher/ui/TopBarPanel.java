package net.minecraft.launcher.ui;

import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.ui.top.ProfileSelectionPanelTop;
import net.minecraft.launcher.ui.top.SetupServerButtonPanel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;

public class TopBarPanel extends /*Textured*/JPanel {
    private final Launcher launcher;
    private final ProfileSelectionPanelTop profileSelectionPanel;
    
    private final SetupServerButtonPanel serverpanel;
    
public TopBarPanel(Launcher launcher) {
		//super("/resources/icons/dirt2.png");
        this.launcher = launcher;
       // this.setOpaque(false);
        int border = 4;
        setBorder(new EmptyBorder(border, border, border, border));

        this.profileSelectionPanel = new ProfileSelectionPanelTop(launcher);        
        
        this.serverpanel = new SetupServerButtonPanel(launcher);

        createInterface();
    }

    protected void createInterface() {
        setLayout(new GridLayout(1, 3));
        
        add(wrapSidePanel(this.profileSelectionPanel, 17));        
      //no server creation  
        add(wrapSidePanel(this.serverpanel, 13));
    }

    protected JPanel wrapSidePanel(JPanel target, int side) {
        JPanel wrapper = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = side;
        constraints.weightx = 1.0D;
        constraints.weighty = 1.0D;


        wrapper.add(target, constraints);

        return wrapper;
    }

    public Launcher getLauncher() {
        return this.launcher;
    }

    public ProfileSelectionPanelTop getProfileSelectionPanel() {
        return this.profileSelectionPanel;
    }

    public SetupServerButtonPanel getServerinstallPanel() {
        return this.serverpanel;
    }
//
//    public PlayButtonPanel getPlayButtonPanel() {
//        return this.playButtonPanel;
//    }
//    
//    public StatusPanelForm getBLPanel() {
//        return this.blPanel;
//    }

}