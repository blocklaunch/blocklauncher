 package net.minecraft.launcher.ui.bottombar;
 
 import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import net.minecraft.launcher.Launcher;
import net.minecraft.launcher.ui.tabs.LauncherTabPanel;

import org.apache.logging.log4j.LogManager;
import org.blocklaunch.mcserverapi.ServiceInformation;
 
 @SuppressWarnings("serial")
public class StatusPanelForm extends SidebarGridForm implements ActionListener
 {
	 //private static final Logger LOGGER = LogManager.getLogger();
   
   private static final String site[] = new String[6];
   private final ImageIcon[] status = new ImageIcon[4];
   private final JLabel[] name = new JLabel[6];

   @SuppressWarnings("unused")
   private final Launcher launcher;

   public StatusPanelForm(Launcher launcher)
   {
     this.launcher = launcher;  
     
     site[0] = "session.minecraft.net";
     site[1] = "auth.mojang.com";
     site[2] = "account.mojang.com";
     site[3] = "authserver.mojang.com";   
     site[4] = "minecraft.net";
     site[5] = "skins.minecraft.net";
     
     name[0]= new JLabel("Multiplayer", 2);
     name[1]= new JLabel("Login", 2);
     name[2]= new JLabel("Account", 2);
     name[3]= new JLabel("Auth", 2);
     name[4]= new JLabel("Website", 2);
     name[5]= new JLabel("Skins", 2);
     
     status[0] = new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/lamp/gray.png"));
     status[1] = new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/lamp/red.png"));
     status[2] = new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/lamp/yellow.png"));
     status[3] = new ImageIcon(LauncherTabPanel.class.getResource("/resources/icons/lamp/green.png"));
 
     createInterface();
     //does the autorefresher
     //refreshStatuses("init");
	
     Timer refresher = new Timer(1, this);
     refresher.setDelay(60000);
     refresher.start();
     
     
   }
 
   protected void populateGrid(GridBagConstraints constraints)
   {
	  
	   for(int i = 0; i<6; i++){
		   name[i].setIcon(status[0]);
	   }	   
     add(name[0] , constraints, 0, 0, 5, 1, 17); 
     add(name[1], constraints, 1, 0, 0, 1, 17);
     add(name[2], constraints, 0, 1, 0, 1, 17);
     add(name[3], constraints, 1, 1, 0, 1, 17);
     add(name[4], constraints, 0, 2, 0, 1, 17);     
     add(name[5], constraints, 1, 2, 0, 1, 17);
     JLabel power = new JLabel("powered by mcstatus.sweetcode.de", SwingConstants.CENTER);
     power.setForeground(Color.GRAY);   
     add(power, constraints, 0, 3, 0, 2, 17);
//     JButton refresh = new JButton("Refresh");
//     refresh.addActionListener(new ActionListener(){
//    	@Override
//		public void actionPerformed(ActionEvent e) {
//			StatusPanelForm.this.refreshStatuses();			
//		}
//     });
//     add(refresh, constraints, 0, 4, 0, 2, 17);
   }
 
   public ImageIcon getSessionStatus() {
     return status[0];
   }
 
   public ImageIcon getLoginStatus() {
     return status[1];
   }
   
   public ImageIcon getAccountStatus() {
	     return status[2];
   }
   public ImageIcon getAuthStatus() {
	     return status[3];
   }
   public ImageIcon getWebsiteStatus() {
	     return status[4];
   }
   public ImageIcon getSkinStatus() {
	     return status[5];
   }
 
   public void refreshStatuses(){
	   LogManager.getLogger().info("Refreshing status of servers");
	   try{
	   for(int i = 0; i<6; i++){
		   ServiceInformation info = new ServiceInformation(site[i]);
		   info.fetchData();  
		   String serverstatus = info.getStatus();
		   StatusPanelForm.this.name[i].setIcon(status[ServerStatus.valueOf(serverstatus).icon]);
		   if(ServerStatus.valueOf(serverstatus.toString()).down){
			   StatusPanelForm.this.name[i].setToolTipText(
					   "<html>"+ServerStatus.valueOf(serverstatus.toString()).title
					   		+ "<br> Server down since "+info.getCurrentDowntime()+" Minutes</html>");
		   }else{
			   StatusPanelForm.this.name[i].setToolTipText(
					   ServerStatus.valueOf(serverstatus.toString()).title);
		   }
	   }
	   }catch(IOException e){
		   e.printStackTrace();
	   }
	   
	   
   }   

   public void actionPerformed(ActionEvent e){
	   StatusPanelForm.this.refreshStatuses();
   }
   
 
   public static enum ServerStatus {
     Online("Online, no problems detected.", 3, false), 
     Unstable("May be experiencing issues.", 2, false), 
     Offline("Offline, experiencing problems.", 1, true),
     unknown("Unknown", 0, false);
 
     private final String title;
     private final int icon; 
     private final boolean down;
 
     private ServerStatus(String title, int icon, boolean down) {
       this.title = title;
       this.icon = icon;
       this.down = down;
     }
   }
 }